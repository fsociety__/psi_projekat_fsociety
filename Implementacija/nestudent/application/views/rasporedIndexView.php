
<!-- Milan Lazarevic 617/14 -->

<div class="container-fluid">
    
    <form style="display:none">           <!------ nevidljivi elementi u koje se ucitavaju podaci o aktivnostima ---->
        <select id="subject_list">
            <?php foreach($info as $row)
                echo "<option>".$row->Naziv."</option>"
            ?>
        </select>
        <select id="p1">
            <?php foreach($info as $row){
                if ($row->P1==NULL)
                    $p="-1";
                else 
                    $p=$row->P1;
                echo "<option value=".$p."></option>";
            }
            ?>
        </select>
        <select id="p2">
            <?php foreach($info as $row){
                if ($row->P2==NULL)
                    $p="-1";
                else 
                    $p=$row->P2;
                echo "<option value=".$p."></option>";
            }
            ?>
        </select>
        <select id="p3">
           <?php foreach($info as $row){
                if ($row->P3==NULL)
                    $p="-1";
                else 
                    $p=$row->P3;
                echo "<option value=".$p."></option>";
           }          
            ?>
        </select>
        <select id="v1">
            <?php foreach($info as $row){
                 if ($row->V1==NULL)
                    $p="-1";
                else 
                    $p=$row->V1;
                echo "<option value=".$p."></option>";
            }
            ?>
        </select>
        <select id="v2">
            <?php foreach($info as $row){
                 if ($row->V2==NULL)
                    $p="-1";
                else 
                    $p=$row->V2;
                echo "<option value=".$p."></option>";
            }
            ?>
        </select>
        <select id="v3">
           <?php foreach($info as $row){
                if ($row->V3==NULL)
                    $p="-1";
                else 
                    $p=$row->V3;
                echo "<option value=".$p."></option>";
           }
            ?>
        </select>
        <select id="bp1">
            <?php foreach($info as $row){
                 if ($row->BP1==NULL)
                    $p="-1";
                else 
                    $p=$row->BP1;
                echo "<option value=".$p."></option>";
            }
            ?>
        </select>
        <select id="bp2">
            <?php foreach($info as $row){
                if ($row->BP2==NULL)
                    $p="-1";
                else 
                    $p=$row->BP2;
                echo "<option value=".$p."></option>";
            }
            ?>
        </select>
        <select id="bp3">
           <?php foreach($info as $row){
                if ($row->BP3==NULL)
                    $p="-1";
                else 
                    $p=$row->BP3;
                echo "<option value=".$p."></option>";
            }
            ?>
        </select>
        <select id="bv1">
            <?php foreach($info as $row){
                 if ($row->BV1==NULL)
                    $p="-1";
                else 
                    $p=$row->BV1;
                echo "<option value=".$p."></option>";
            }
            ?>
        </select>
        <select id="bv2">
            <?php foreach($info as $row){
                 if ($row->BV2==NULL)
                    $p="-1";
                else 
                    $p=$row->BV2;
                echo "<option value=".$p."></option>";
            }
            ?>
        </select>
        <select id="bv3">
           <?php foreach($info as $row){
                if ($row->BV3==NULL)
                    $p="-1";
                else 
                    $p=$row->BV3;
                echo "<option value=".$p."></option>";
            }
            ?>
        </select>
    </form>
 
    <div class="col-md-12">
        <div class="col-md-2">
            <?php echo form_open('rasporedController/izmeniRaspored'); ?>
                <button type="submit" class="btn btn-white btn-round btn-just-icon">
                    <i class="material-icons">edit</i><div class="ripple-container"></div>
                </button>
            </form>
        </div>
    </div>
    
    <div class="col-md-12">    
        <div class="col-md-2">
            <div class="card card-plain">
                <div class="card-header" data-background-color="purple">
                    <h4 class="title">Ponedeljak</h4>
                    <p class="category"></p>
                </div>
                <div class="card-content table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <th>Predmet</th>
                            <th>Vreme</th>
                            <th>P/V</th>
                        </thead>
                        <tbody id="ponedeljak">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-md-2">
            <div class="card card-plain">
                <div class="card-header" data-background-color="purple">
                    <h4 class="title">Utorak</h4>
                    <p class="category"></p>
                </div>
                <div class="card-content table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <th>Predmet</th>
                            <th>Vreme</th>
                            <th>P/V</th>
                        </thead>
                        <tbody id="utorak">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

            <div class="col-md-2">
            <div class="card card-plain">
                <div class="card-header" data-background-color="purple">
                    <h4 class="title">Sreda</h4>
                    <p class="category"></p>
                </div>
                <div class="card-content table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <th>Predmet</th>
                            <th>Vreme</th>
                            <th>P/V</th>
                        </thead>
                        <tbody id="sreda">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

            <div class="col-md-2">
            <div class="card card-plain">
                <div class="card-header" data-background-color="purple">
                    <h4 class="title">Cetvrtak</h4>
                    <p class="category"></p>
                </div>
                <div class="card-content table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <th>Predmet</th>
                            <th>Vreme</th>
                            <th>P/V</th>
                        </thead>
                        <tbody id="cetvrtak">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-md-2">
            <div class="card card-plain">
                <div class="card-header" data-background-color="purple">
                    <h4 class="title">Petak</h4>
                    <p class="category"></p>
                </div>
                <div class="card-content table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <th>Predmet</th>
                            <th>Vreme</th>
                            <th>P/V</th>
                        </thead>
                        <tbody id="petak">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>
<script>
    
    function ubacivanje(namep, boolak, ak, pv){     //funkcija za ucitavanje aktivnosti koje student je izabrao da mu se vide na rasporedu
        var infoObj=document.getElementById(boolak);        //po danima u nizove pon, uto, sre, cet, pet
        var nameObj=document.getElementById(namep);
        var termObj=document.getElementById(ak);
        var i=0;
        var pom;
        var pom1=[];
        var pom3=[];
        var pom4;
        for (i=0; i<infoObj.length; i++){
            if (new String(infoObj[i].value).valueOf()==new String("0").valueOf()){
                infoObj[i].value="-1";
            }
            if (new String(infoObj[i].value).valueOf()!=new String("-1").valueOf()){
                pom1=(termObj[i].value).split("_");
                pom=parseInt(pom1[0],10);
                switch(pom){
                    case 1:
                        pom4=nameObj[i].innerHTML+"_"+pom1[1]+"_"+pv;
                        pon.push(pom4);
                        break;
                    case 2:
                        pom4=nameObj[i].innerHTML+"_"+pom1[1]+"_"+pv;
                        uto.push(pom4);
                        break;
                    case 3:
                        pom4=nameObj[i].innerHTML+"_"+pom1[1]+"_"+pv;
                        sre.push(pom4);
                        break;
                    case 4:
                        pom4=nameObj[i].innerHTML+"_"+pom1[1]+"_"+pv;
                        cet.push(pom4);
                        break;
                    case 5:
                        pom4=nameObj[i].innerHTML+"_"+pom1[1]+"_"+pv;
                        pet.push(pom4);
                        break;
                }
            } 
        }
    }
    
    function compareFunction(a, b){                     //funkcija sa kojom se sortiraju nizovi u rasporedu
        var pom=a.split("_");
        var pom1=b.split("_");
        pom=pom[1].split(":");
        pom1=pom1[1].split(":");
        return (parseInt(pom[0],10)*60+parseInt(pom[1],10))-(parseInt(pom1[0])*60+parseInt(pom1[1],10));
    }
    
    var nameOb=document.getElementById("subject_list");
    var pon=[];
    var uto=[];
    var sre=[];
    var cet=[];
    var pet=[];
    var i;
    var textInject;
    var pomText;
    
    ubacivanje("subject_list","bp1","p1", "P");         //ubacivanje u nizove 
    ubacivanje("subject_list","bp2","p2", "P");
    ubacivanje("subject_list","bp3","p3", "P");
    ubacivanje("subject_list","bv1","v1", "V");
    ubacivanje("subject_list","bv2","v2", "V");
    ubacivanje("subject_list","bv3","v3", "V");
    pon.sort(compareFunction);                          //sortiranje nizova
    uto.sort(compareFunction);
    sre.sort(compareFunction);
    cet.sort(compareFunction);
    pet.sort(compareFunction);
    
    textInject="";
    for (i=0; i<pon.length;i++){                        //ubacivanje informacija iz nizova u tabele za prikaz rasporeda
        pomText=pon[i].split("_");
        textInject=textInject+"<tr>";
        textInject=textInject+"<td>";
        textInject=textInject+pomText[0];
        textInject=textInject+"</td>";
        textInject=textInject+"<td>";
        textInject=textInject+pomText[1];
        textInject=textInject+"</td>";
        textInject=textInject+"<td>";
        textInject=textInject+pomText[2];
        textInject=textInject+"</td>";
        textInject=textInject+"</tr>";
    }
    document.getElementById("ponedeljak").innerHTML=textInject;
    
    textInject="";
    for (i=0; i<uto.length;i++){
        pomText=uto[i].split("_");
        textInject=textInject+"<tr>";
        textInject=textInject+"<td>";
        textInject=textInject+pomText[0];
        textInject=textInject+"</td>";
        textInject=textInject+"<td>";
        textInject=textInject+pomText[1];
        textInject=textInject+"</td>";
        textInject=textInject+"<td>";
        textInject=textInject+pomText[2];
        textInject=textInject+"</td>";
        textInject=textInject+"</tr>";
    }
    document.getElementById("utorak").innerHTML=textInject;
    
    textInject="";
    for (i=0; i<sre.length;i++){
        pomText=sre[i].split("_");
        textInject=textInject+"<tr>";
        textInject=textInject+"<td>";
        textInject=textInject+pomText[0];
        textInject=textInject+"</td>";
        textInject=textInject+"<td>";
        textInject=textInject+pomText[1];
        textInject=textInject+"</td>";
        textInject=textInject+"<td>";
        textInject=textInject+pomText[2];
        textInject=textInject+"</td>";
        textInject=textInject+"</tr>";
    }
    document.getElementById("sreda").innerHTML=textInject;
    
    textInject="";
    for (i=0; i<cet.length;i++){
        pomText=cet[i].split("_");
        textInject=textInject+"<tr>";
        textInject=textInject+"<td>";
        textInject=textInject+pomText[0];
        textInject=textInject+"</td>";
        textInject=textInject+"<td>";
        textInject=textInject+pomText[1];
        textInject=textInject+"</td>";
        textInject=textInject+"<td>";
        textInject=textInject+pomText[2];
        textInject=textInject+"</td>";
        textInject=textInject+"</tr>";
    }
    document.getElementById("cetvrtak").innerHTML=textInject;
    
    textInject="";
    for (i=0; i<pet.length;i++){
        pomText=pet[i].split("_");
        textInject=textInject+"<tr>";
        textInject=textInject+"<td>";
        textInject=textInject+pomText[0];
        textInject=textInject+"</td>";
        textInject=textInject+"<td>";
        textInject=textInject+pomText[1];
        textInject=textInject+"</td>";
        textInject=textInject+"<td>";
        textInject=textInject+pomText[2];
        textInject=textInject+"</td>";
        textInject=textInject+"</tr>";
    }
    document.getElementById("petak").innerHTML=textInject;
</script>
