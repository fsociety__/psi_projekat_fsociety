<!-- Emanuilo Jovanovic 563/14 -->

<div class="container-fluid">

    <div class="col-lg-9 col-md-12">
        <div class="card">
            <div class="card-header" data-background-color="green">
                <h4 class="title">Izabrati predmet</h4>

            </div>
            <div class="card-content table-responsive">
                <table class="table table-hover">
                    <tbody>
                        <?php $cnt = 1; ?>
                        <?php foreach($predmeti as $predmet) { ?>
                        <tr class='clickable-row' onclick="fja(<?php echo $predmet->IDPre ?>);">
                                <td><?php echo $cnt++ ?></td>
                                <td><span role="button"><h4 class="card-title"><?php echo $predmet->Naziv ?></h4></span></td>

                            
                        <?php } ?>
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</div>

<script type="text/javascript">
    
    function fja(idPre){
        window.open('<?php echo base_url()?>index.php/<?php echo $adresa ?>/' + idPre, "_self");
    }
    
</script>