
<!-- Milan Lazarevic 617/14 -->

<div class="container-fluid">
    
            <!------ nevidljivi elementi u koje se ucitavaju podaci o aktivnostima ---->
    <div style="display:none">
        <select id="identifikacija">
            <?php foreach($info as $row)
                echo "<option value=".$row->IdPre."></option>"
            ?>
        </select>
        <select id="subject_list">
            <?php foreach($info as $row)
                echo "<option>".$row->Naziv."</option>"
            ?>
        </select>
        <select id="p1">
            <?php foreach($info as $row){
                if ($row->P1==NULL)
                    $p="-1";
                else 
                    $p=$row->P1;
                echo "<option value=".$p."></option>";
            }
            ?>
        </select>
        <select id="p2">
            <?php foreach($info as $row){
                if ($row->P2==NULL)
                    $p="-1";
                else 
                    $p=$row->P2;
                echo "<option value=".$p."></option>";
            }
            ?>
        </select>
        <select id="p3">
           <?php foreach($info as $row){
                if ($row->P3==NULL)
                    $p="-1";
                else 
                    $p=$row->P3;
                echo "<option value=".$p."></option>";
           }          
            ?>
        </select>
        <select id="v1">
            <?php foreach($info as $row){
                 if ($row->V1==NULL)
                    $p="-1";
                else 
                    $p=$row->V1;
                echo "<option value=".$p."></option>";
            }
            ?>
        </select>
        <select id="v2">
            <?php foreach($info as $row){
                 if ($row->V2==NULL)
                    $p="-1";
                else 
                    $p=$row->V2;
                echo "<option value=".$p."></option>";
            }
            ?>
        </select>
        <select id="v3">
           <?php foreach($info as $row){
                if ($row->V3==NULL)
                    $p="-1";
                else 
                    $p=$row->V3;
                echo "<option value=".$p."></option>";
           }
            ?>
        </select>
        <select id="bp1">
            <?php foreach($info as $row){
                 if ($row->BP1==NULL)
                    $p="-1";
                else 
                    $p=$row->BP1;
                echo "<option value=".$p."></option>";
            }
            ?>
        </select>
        <select id="bp2">
            <?php foreach($info as $row){
                if ($row->BP2==NULL)
                    $p="-1";
                else 
                    $p=$row->BP2;
                echo "<option value=".$p."></option>";
            }
            ?>
        </select>
        <select id="bp3">
           <?php foreach($info as $row){
                if ($row->BP3==NULL)
                    $p="-1";
                else 
                    $p=$row->BP3;
                echo "<option value=".$p."></option>";
            }
            ?>
        </select>
        <select id="bv1">
            <?php foreach($info as $row){
                 if ($row->BV1==NULL)
                    $p="-1";
                else 
                    $p=$row->BV1;
                echo "<option value=".$p."></option>";
            }
            ?>
        </select>
        <select id="bv2">
            <?php foreach($info as $row){
                 if ($row->BV2==NULL)
                    $p="-1";
                else 
                    $p=$row->BV2;
                echo "<option value=".$p."></option>";
            }
            ?>
        </select>
        <select id="bv3">
           <?php foreach($info as $row){
                if ($row->BV3==NULL)
                    $p="-1";
                else 
                    $p=$row->BV3;
                echo "<option value=".$p."></option>";
            }
            ?>
        </select>
    </div> 
            
    <div class="col-md-12"> 
        <?php echo form_open('rasporedController/sacuvajRaspored'); ?>
            <div style="display:none">
                <input type="text" value="" name="identifikacija" id="identifikacijat"/>
                <input type="text" value="" name="bp1" id="bp1t"/>
                <input type="text" value="" name="bp2" id="bp2t"/>
                <input type="text" value="" name="bp3" id="bp3t"/>
                <input type="text" value="" name="bv1" id="bv1t"/> 
                <input type="text" value="" name="bv2" id="bv2t"/>
                <input type="text" value="" name="bv3" id="bv3t"/>
            </div>
            <div class="col-md-2">
                    <button type="submit" class="btn btn-white btn-round btn-just-icon">
                        <i class="material-icons">save</i><div class="ripple-container"></div>
                    </button>
            </div>
        </form>
    </div>
        
    <div class="col-md-12"> 
        
        <div class="col-md-2">
            <div class="card card-plain">
                <div class="card-header" data-background-color="blue">
                    <h4 class="title">Predmeti</h4>
                    <p class="category"></p>
                </div>
                <div class="card-content table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <th>Naziv</th>
                            <th>P</th>
                            <th>V</th>
                        </thead>
                        <tbody id="predmeti">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        
        <div class="col-md-2">
            <div class="card card-plain">
                <div class="card-header" data-background-color="purple">
                    <h4 class="title">Ponedeljak</h4>
                    <p class="category"></p>
                </div>
                <div class="card-content table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <th>Predmet</th>
                            <th>Vreme</th>
                            <th>P/V</th>
                        </thead>
                        <tbody id="ponedeljak">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-md-2">
            <div class="card card-plain">
                <div class="card-header" data-background-color="purple">
                    <h4 class="title">Utorak</h4>
                    <p class="category"></p>
                </div>
                <div class="card-content table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <th>Predmet</th>
                            <th>Vreme</th>
                            <th>P/V</th>
                        </thead>
                        <tbody id="utorak">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

            <div class="col-md-2">
            <div class="card card-plain">
                <div class="card-header" data-background-color="purple">
                    <h4 class="title">Sreda</h4>
                    <p class="category"></p>
                </div>
                <div class="card-content table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <th>Predmet</th>
                            <th>Vreme</th>
                            <th>P/V</th>
                        </thead>
                        <tbody id="sreda">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

            <div class="col-md-2">
            <div class="card card-plain">
                <div class="card-header" data-background-color="purple">
                    <h4 class="title">Cetvrtak</h4>
                    <p class="category"></p>
                </div>
                <div class="card-content table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <th>Predmet</th>
                            <th>Vreme</th>
                            <th>P/V</th>
                        </thead>
                        <tbody id="cetvrtak">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-md-2">
            <div class="card card-plain">
                <div class="card-header" data-background-color="purple">
                    <h4 class="title">Petak</h4>
                    <p class="category"></p>
                </div>
                <div class="card-content table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <th>Predmet</th>
                            <th>Vreme</th>
                            <th>P/V</th>
                        </thead>
                        <tbody id="petak">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>
<script>
    
    function ubacivanje(namep, boolak, ak, pv){     //funkcija za ucitavanje aktivnosti koje student je izabrao da mu se vide na rasporedu
        var infoObj=document.getElementById(boolak);        //po danima u nizove pon, uto, sre, cet, pet
        var nameObj=document.getElementById(namep);
        var termObj=document.getElementById(ak);
        var povratniObj=document.getElementById(boolak+"t");
        var i=0;
        var pom;
        var pom1=[];
        var pom3=[];
        var pom4;
        var temp1;
        var controllerRet="";
        for (i=0; i<infoObj.length; i++){
            if (new String(infoObj[i].value).valueOf()==new String("0").valueOf()){
                infoObj[i].value="-1";
            }
            if (new String(infoObj[i].value).valueOf()==new String("-1").valueOf())
                temp1="0";
            else
                temp1=infoObj[i].value;
            controllerRet+=temp1+"_";
            if (new String(infoObj[i].value).valueOf()!=new String("-1").valueOf()){
                pom1=(termObj[i].value).split("_");
                pom=parseInt(pom1[0],10);
                switch(pom){
                    case 1:
                        pom4=nameObj[i].innerText+"_"+pom1[1]+"_"+pv+"_"+i;
                        pon.push(pom4);
                        break;
                    case 2:
                        pom4=nameObj[i].innerText+"_"+pom1[1]+"_"+pv+"_"+i;
                        uto.push(pom4);
                        break;
                    case 3:
                        pom4=nameObj[i].innerText+"_"+pom1[1]+"_"+pv+"_"+i;
                        sre.push(pom4);
                        break;
                    case 4:
                        pom4=nameObj[i].innerText+"_"+pom1[1]+"_"+pv+"_"+i;
                        cet.push(pom4);
                        break;
                    case 5:
                        pom4=nameObj[i].innerText+"_"+pom1[1]+"_"+pv+"_"+i;
                        pet.push(pom4);
                        break;
                }
            } 
        }
        povratniObj.value=controllerRet;
    }
    
    function proveraChecked(id, flag){
        var bp1=document.getElementById("bp1");
        var bp2=document.getElementById("bp2");
        var bp3=document.getElementById("bp3");
        var bv1=document.getElementById("bv1");
        var bv2=document.getElementById("bv2");
        var bv3=document.getElementById("bv3");
        if (flag==1){
            if (new String(bp1[id].value).valueOf()!=new String("-1").valueOf())
                return true;
            if (new String(bp2[id].value).valueOf()!=new String("-1").valueOf())
                return true;
            if (new String(bp3[id].value).valueOf()!=new String("-1").valueOf())
                return true;
            return false;
        }
        else{
            if (new String(bv1[id].value).valueOf()!=new String("-1").valueOf())
                return true;
            if (new String(bv2[id].value).valueOf()!=new String("-1").valueOf())
                return true;
            if (new String(bv3[id].value).valueOf()!=new String("-1").valueOf())
                return true;
            return false;
        }
    }
    
    
    function compareFunction(a, b){                     //funkcija sa kojom se sortiraju nizovi u rasporedu
        var pom=a.split("_");
        var pom1=b.split("_");
        pom=pom[1].split(":");
        pom1=pom1[1].split(":");
        return (parseInt(pom[0],10)*60+parseInt(pom[1],10))-(parseInt(pom1[0])*60+parseInt(pom1[1],10));
    }
    
    var nameOb=document.getElementById("subject_list");
    var pon=[];
    var uto=[];
    var sre=[];
    var cet=[];
    var pet=[];
    var i;
    var textInject;
    var pomText;
    
    glavna();
    
    function glavna(){
        var identOb=document.getElementById("identifikacija");
        nameOb=document.getElementById("subject_list");
        pon=[];
        uto=[];
        sre=[];
        cet=[];
        pet=[];
        ubacivanje("subject_list","bp1","p1", "P");         //ubacivanje u nizove 
        ubacivanje("subject_list","bp2","p2", "P");
        ubacivanje("subject_list","bp3","p3", "P");
        ubacivanje("subject_list","bv1","v1", "V");
        ubacivanje("subject_list","bv2","v2", "V");
        ubacivanje("subject_list","bv3","v3", "V");
        pon.sort(compareFunction);                          //sortiranje nizova
        uto.sort(compareFunction);
        sre.sort(compareFunction);
        cet.sort(compareFunction);
        pet.sort(compareFunction);
        
        textInject="";
        for (i=0; i<identOb.length; i++){
            textInject+=identOb[i].value+"_";
        }
        document.getElementById("identifikacijat").value=textInject;
        
        textInject="";
        for (i=0; i<pon.length;i++){                        //ubacivanje informacija iz nizova u tabele za prikaz rasporeda
            pomText=pon[i].split("_");
            textInject=textInject+'<tr onclick="rasporedRow(this);" id="nesto'+i+'_1_'+pomText[3]+'">';
            textInject=textInject+"<td>";
            textInject=textInject+pomText[0];
            textInject=textInject+"</td>";
            textInject=textInject+"<td>";
            textInject=textInject+pomText[1];
            textInject=textInject+"</td>";
            textInject=textInject+"<td>";
            textInject=textInject+pomText[2];
            textInject=textInject+"</td>";
            textInject=textInject+"</tr>";
        }
        document.getElementById("ponedeljak").innerHTML=textInject;

        textInject="";
        for (i=0; i<uto.length;i++){
            pomText=uto[i].split("_");
            textInject=textInject+'<tr onclick="rasporedRow(this);" id="nesto'+i+'_2_'+pomText[3]+'">';
            textInject=textInject+"<td>";
            textInject=textInject+pomText[0];
            textInject=textInject+"</td>";
            textInject=textInject+"<td>";
            textInject=textInject+pomText[1];
            textInject=textInject+"</td>";
            textInject=textInject+"<td>";
            textInject=textInject+pomText[2];
            textInject=textInject+"</td>";
            textInject=textInject+"</tr>";
        }
        document.getElementById("utorak").innerHTML=textInject;

        textInject="";
        for (i=0; i<sre.length;i++){
            pomText=sre[i].split("_");
            textInject=textInject+'<tr onclick="rasporedRow(this);" id="nesto'+i+'_3_'+pomText[3]+'">';
            textInject=textInject+"<td>";
            textInject=textInject+pomText[0];
            textInject=textInject+"</td>";
            textInject=textInject+"<td>";
            textInject=textInject+pomText[1];
            textInject=textInject+"</td>";
            textInject=textInject+"<td>";
            textInject=textInject+pomText[2];
            textInject=textInject+"</td>";
            textInject=textInject+"</tr>";
        }
        document.getElementById("sreda").innerHTML=textInject;

        textInject="";
        for (i=0; i<cet.length;i++){
            pomText=cet[i].split("_");
            textInject=textInject+'<tr onclick="rasporedRow(this);" id="nesto'+i+'_4_'+pomText[3]+'">';
            textInject=textInject+"<td>";
            textInject=textInject+pomText[0];
            textInject=textInject+"</td>";
            textInject=textInject+"<td>";
            textInject=textInject+pomText[1];
            textInject=textInject+"</td>";
            textInject=textInject+"<td>";
            textInject=textInject+pomText[2];
            textInject=textInject+"</td>";
            textInject=textInject+"</tr>";
        }
        document.getElementById("cetvrtak").innerHTML=textInject;
        
        textInject="";
        for (i=0; i<pet.length;i++){
            pomText=pet[i].split("_");
            textInject=textInject+'<tr onclick="rasporedRow(this);" id="nesto'+i+'_5_'+pomText[3]+'">';
            textInject=textInject+"<td>";
            textInject=textInject+pomText[0];
            textInject=textInject+"</td>";
            textInject=textInject+"<td>";
            textInject=textInject+pomText[1];
            textInject=textInject+"</td>";
            textInject=textInject+"<td>";
            textInject=textInject+pomText[2];
            textInject=textInject+"</td>";
            textInject=textInject+"</tr>";
            
            /* textinject = 
             
             `<tr>
            <td>${pomText[0]}</td>
            <td>${pomText[1]}</td>
            <td>${pomText[2]}</td>
               </tr>`
             
                */
        }
        document.getElementById("petak").innerHTML=textInject;

        textInject="";
        for (i=0; i<nameOb.length;i++){
            textInject=textInject+"<tr>";
            textInject=textInject+"<td>";
            textInject=textInject+nameOb[i].innerHTML;
            textInject=textInject+"</td>";
            textInject=textInject+"<td>";
            if (proveraChecked(i,1))
                textInject+='<input type="checkbox" id="'+nameOb[i].innerHTML+'P" checked onclick="predmetbox('+i+',1, this);"/>';
            else
                textInject+='<input type="checkbox" id="'+nameOb[i].innerHTML+'P" onclick="predmetbox('+i+',1, this);"/>';
            textInject=textInject+"</td>";
            textInject=textInject+"<td>";
            if (proveraChecked(i,0))
                textInject+='<input type="checkbox" id="'+nameOb[i].innerHTML+'V" checked onclick="predmetbox('+i+',0, this);"/>';
            else
                textInject+='<input type="checkbox" id="'+nameOb[i].innerHTML+'V" onclick="predmetbox('+i+',0, this);"/>';
            textInject=textInject+"</td>";
            textInject=textInject+"</tr>";
        }
        document.getElementById("predmeti").innerHTML=textInject;
    }
    
    function predmetbox(otac, pv, object){
        var ar1=[];
        var ar2=[];
        ar1.push(document.getElementById("p1"));
        ar1.push(document.getElementById("p2"));
        ar1.push(document.getElementById("p3"));
        ar1.push(document.getElementById("v1"));
        ar1.push(document.getElementById("v2"));
        ar1.push(document.getElementById("v3"));
        ar2.push(document.getElementById("bp1"));
        ar2.push(document.getElementById("bp2"));
        ar2.push(document.getElementById("bp3"));
        ar2.push(document.getElementById("bv1"));
        ar2.push(document.getElementById("bv2"));
        ar2.push(document.getElementById("bv3"));
        var j=0;
        var max=6;
        if (pv==1)
            max=3;
        else
            j=3;
        if (object.checked){
            for (; j<max; j++){
                if (new String(ar1[j][otac].value).valueOf()!=new String("-1").valueOf())
                    ar2[j][otac].value="1";
                else 
                    ar2[j][otac].value="-1";
            }
        }
        else{
            for (; j<max; j++){
                ar2[j][otac].value="-1";
            }
        }
        glavna();
    }
    
    function rasporedRow(object){
        nameOb=document.getElementById("subject_list");
        var ime=object.cells[0].innerText;
        var vreme=object.cells[1].innerText;
        var tmep=object.id.split("_");
        i=parseInt(tmep[2],10);
        vreme=tmep[1]+"_"+vreme;
        var pv=object.cells[2].innerText;
        var ar1=[];
        var ar2=[];
        var j=0;
        ar1.push(document.getElementById("p1"));
        ar1.push(document.getElementById("p2"));
        ar1.push(document.getElementById("p3"));
        ar1.push(document.getElementById("v1"));
        ar1.push(document.getElementById("v2"));
        ar1.push(document.getElementById("v3"));
        ar2.push(document.getElementById("bp1"));
        ar2.push(document.getElementById("bp2"));
        ar2.push(document.getElementById("bp3"));
        ar2.push(document.getElementById("bv1"));
        ar2.push(document.getElementById("bv2"));
        ar2.push(document.getElementById("bv3"));
       // for (i=0;i<nameOb.length;i++){
        //    if (new String(ime).valueOf()==new String(nameOb[i].innerText).valueOf())
          //      break;
       // }
        if (i!=nameOb.length){
            if (new String(pv).valueOf()!=new String("V").valueOf()){
                for (j=0;j<3;j++){
                    if (new String(ar1[j][i].value).valueOf()==new String(vreme).valueOf()){
                        ar2[j][i].value="-1";
                        break;
                    }
                }
            }
            else{
                for (j=3;j<6;j++){
                    if (new String(ar1[j][i].value).valueOf()==new String(vreme).valueOf()){
                        ar2[j][i].value="-1";
                        break;
                    }
                }
            }
        }
        glavna();
    }
    
    
</script>
