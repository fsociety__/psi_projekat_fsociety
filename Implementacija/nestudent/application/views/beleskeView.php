<!--Aleksa Jankovic 2014 0569-->


<div class="container-fluid">
    <div class="row">
        <a href="<?php echo site_url('beleskeController/addBeleska/') ?>">
            <button class="btn btn-info btn-lg" style="float: right; margin-bottom: 20px">
                <i class="material-icons">
                    add
                </i>
                Nova Beleska
            </button>
        </a>
    </div>
</div>

<div class="row">
    <?php foreach ($beleske as $beleska) { ?>

        <!-- primer check liste -->
        <div class="col-lg-3 col-md-12">
            <div class="card card-nav-tabs">

                <!-- Naslov belekse -->
                <div class="card-header" data-background-color="blue">
                    <div class="nav-tabs-navigation">
                        <div class="nav-tabs-wrapper">
                            <span class="nav-tabs-title" id="<?php echo $beleska['IDBel'] ?>" name="<?php echo $beleska['IDBel'] ?>">
                                <?php echo $beleska['Naslov'] ?>
                            </span>
                            <ul class="nav nav-tabs" data-tabs="tabs">
                                <!-- ovde su bile one ikonice server bugs..-->
                                <!-- Edit && Delete -->
                                <div class="text-right" style="float: right">
                                    <a rel="tooltip" title="Edit Note" class="btn btn-default btn-simple btn-xs" href="<?php echo site_url('beleskeController/editBeleska/' . $beleska['IDBel']) ?>">
                                        <i class="material-icons">edit</i>
                                    </a>
                                    <a type="button" rel="tooltip" title="Remove" class="btn btn-danger btn-simple btn-xs" href="<?php echo site_url('beleskeController/deleteBeleska/' . $beleska['IDBel']) ?>">
                                        <i class="material-icons">close</i>
                                    </a>
                                </div>
                            </ul>

                        </div>
                    </div>
                </div>

                <?php if ($beleska['isCheckBox'] == 0) { ?>
                    <!-- Sadrzaj tekst beleske -->
                    <div class="card-content">
                        <div class="tab-content">
                            <div class="tab-pane active" id="profile2">
                                <span>
                                    <?php if (isset($beleska['stavke'])) { ?>                                        
                                        <?php
                                        $arr = explode("\n", $beleska['stavke'][0]['Tekst']);
                                        foreach ($arr as $strRow){
                                            echo $strRow;
                                            echo '<br/>';
                                        }
                                        ?>
                                    <?php } ?>
                                </span>
                            </div>
                        </div>
                    </div>

                <?php } else { ?> 
                    <!-- Sadrzaj check beleske -->
                    <div class="card-content">
                        <div class="tab-content">
                            <div class="tab-pane active" id="profile">
                                <table class="table">
                                    <tbody>
                                        <?php if (isset($beleska['stavke'])) { ?>
                                            <?php foreach ($beleska['stavke'] as $stavka) { ?>
                                                <tr>
                                                    <!-- checkbox -->
                                                    <td class="col-lg-1 col-md-1 col-sm-1">
                                                        <div class="checkbox">
                                                            <label>
                                                                <?php if ($stavka['isChecked'] == 1) { ?>
                                                                    <input type="checkbox" name="optionsCheckboxes" disabled checked>
                                                                <?php } else { ?>
                                                                    <input type="checkbox" name="optionsCheckboxes" disabled>
                                                                <?php } ?>
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <!-- Tekst -->
                                                    <td class="col-lg-10 col-md-10 col-sm-10">
                                                        <?php echo $stavka['Tekst'] ?>
                                                    </td>
                                                    <!-- Edit && Delete za svaku stavku posebno -->
                                                    <!--
                                                    <td class="td-actions text-right">
                                                        <button type="button" rel="tooltip" title="Edit Task" class="btn btn-primary btn-simple btn-xs">
                                                            <i class="material-icons">edit</i>
                                                        </button>
                                                        <button type="button" rel="tooltip" title="Remove" class="btn btn-danger btn-simple btn-xs">
                                                            <i class="material-icons">close</i>
                                                        </button>
                                                    </td>
                                                    -->
                                                </tr>
                                            <?php } ?>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    <?php } ?>
</div>
</div>