<!-- Emanuilo Jovanovic 563/14 -->

<div class="container-fluid">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header" data-background-color="blue">
                <h4 class="title">Rezultati</h4>
                <p class="category">Unos rezultata</p>
            </div>
            <div class="card-content table-responsive">
            <?php echo form_open("rezultatiController/unesiRezultate"); ?> 
                <table class="table">
                    <thead class="text-info">
                    <th><h4 class="title">Predmet</h4></th>
                    <th><h4 class="title">K1</h4></th>
                    <th><h4 class="title">K2</h4></th>
                    <th><h4 class="title">K3</h4></th>
                    <th><h4 class="title">Lab1</h4></th>
                    <th><h4 class="title">Lab2</h4></th>
                    <th><h4 class="title">Lab3</h4></th>
                    <th><h4 class="title">Lab4</h4></th>
                    <th><h4 class="title">Lab5</h4></th>
                    <th><h4 class="title">DZ1</h4></th>
                    <th><h4 class="title">DZ2</h4></th>
                    <th><h4 class="title">DZ3</h4></th>
                    <th><h4 class="title">Projekat</h4></th>
                    <th class="text-primary"><h4 class="title">Ukupno</h4></th>
                    <th class="text-warning"><h4 class="title">Ocena</h4></th>
                    </thead>
                    <tbody>
                        <?php $cnt = 1; ?>
                        <?php foreach($predmeti as $index => $predmet) { ?>
                            <?php 
                                $formula = $formule[$index];
                                $sabirci = explode("+", $formula->Formula);

                                $zbir = 0;
                                $K1 = false; $K2 = false; $K3 = false;
                                $L1 = false; $L2 = false; $L3 = false; $L4 = false; $L5 = false;
                                $DZ1 = false; $DZ2 = false; $DZ3 = false; $PR = false;
                                foreach ($sabirci as $sabirak){
                                    $test=""; $umnozak="";
                                    if(strpos($sabirak, '*') !== false){
                                        list($test, $umnozak) = explode('*', $sabirak);
                                    }
                                    else{
                                        $test = $sabirak;
                                        $umnozak = 1;
                                    }
                                    $umnozak = (float)$umnozak;
                                    switch ($test){
                                        case "K1":
                                            $K1 = true;
                                            if($rezultati[$index]->Kolokvijum1 > 0) $zbir += $rezultati[$index]->Kolokvijum1*$umnozak;
                                            break;
                                        case "K2":
                                            $K2 = true;
                                            if($rezultati[$index]->Kolokvijum2 > 0) $zbir += $rezultati[$index]->Kolokvijum2*$umnozak;
                                            break;
                                        case "K3":
                                            $K3 = true;
                                            if($rezultati[$index]->Kolokvijum3 > 0) $zbir += $rezultati[$index]->Kolokvijum3*$umnozak;
                                            break;
                                        case "L1":
                                            $L1 = true;
                                            if($rezultati[$index]->Lab1 > 0) $zbir += $rezultati[$index]->Lab1*$umnozak;
                                            break;
                                        case "L2":
                                            $L2 = true;
                                            if($rezultati[$index]->Lab2 > 0) $zbir += $rezultati[$index]->Lab2*$umnozak;
                                            break;
                                        case "L3":
                                            $L3 = true;
                                            if($rezultati[$index]->Lab3 > 0) $zbir += $rezultati[$index]->Lab3*$umnozak;
                                            break;
                                        case "L4":
                                            $L4 = true;
                                            if($rezultati[$index]->Lab4 > 0) $zbir += $rezultati[$index]->Lab4*$umnozak;
                                            break;
                                        case "L5":
                                            $L5 = true;
                                            if($rezultati[$index]->Lab5 > 0) $zbir += $rezultati[$index]->Lab5*$umnozak;
                                            break;
                                        case "DZ1":
                                            $DZ1 = true;
                                            if($rezultati[$index]->DZ1 > 0) $zbir += $rezultati[$index]->DZ1*$umnozak;
                                            break;
                                        case "DZ2":
                                            $DZ2 = true;
                                            if($rezultati[$index]->DZ2 > 0) $zbir += $rezultati[$index]->DZ2*$umnozak;
                                            break;
                                        case "DZ3":
                                            $DZ3 = true;
                                            if($rezultati[$index]->DZ3 > 0) $zbir += $rezultati[$index]->DZ3*$umnozak;
                                            break;
                                        case "PR":
                                            $PR = true;
                                            if($rezultati[$index]->Projekat > 0) $zbir += $rezultati[$index]->Projekat*$umnozak;
                                            break;

                                    }
                                }
                                $ocena = 5;
                                if($zbir > 0) {
                                    switch($zbir){
                                        case $zbir < 51:
                                            $ocena = 5;
                                            break;
                                        case $zbir >= 51 && $zbir < 61:
                                            $ocena = 6;
                                            break;
                                        case $zbir >= 61 && $zbir < 71:
                                            $ocena = 7;
                                            break;
                                        case $zbir >= 71 && $zbir < 81:
                                            $ocena = 8;
                                            break;
                                        case $zbir >= 81 && $zbir < 91:
                                            $ocena = 9;
                                            break;
                                        case $zbir >= 91:
                                            $ocena = 10;
                                            break;
                                    }
                                    
                                }

                            ?>
                            <tr>
                                <td><?php echo $predmet->Naziv ?></td>
                                <td>
                                    <div class="form-group label-floating">
                                        <label class="control-label"></label>
                                        <input type="text" class="form-control" value='<?php if($rezultati[$index]->Kolokvijum1 > 0) echo $rezultati[$index]->Kolokvijum1 ?>' name='<?php echo 'K1'.$cnt ?>' <?php if(!$K1) echo ' disabled' ?>>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group label-floating">
                                        <label class="control-label"></label>
                                        <input type="text" class="form-control" value='<?php if($rezultati[$index]->Kolokvijum2 > 0) echo $rezultati[$index]->Kolokvijum2 ?>' name='<?php echo 'K2'.$cnt ?>' <?php if(!$K2) echo ' disabled' ?>>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group label-floating">
                                        <label class="control-label"></label>
                                        <input type="text" class="form-control" value='<?php if($rezultati[$index]->Kolokvijum3 > 0) echo $rezultati[$index]->Kolokvijum3 ?>' name='<?php echo 'K3'.$cnt ?>' <?php if(!$K3) echo ' disabled' ?>>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group label-floating">
                                        <label class="control-label"></label>
                                        <input type="text" class="form-control" value='<?php if($rezultati[$index]->Lab1 > 0) echo $rezultati[$index]->Lab1 ?>' name='<?php echo 'L1'.$cnt ?>' <?php if(!$L1) echo ' disabled' ?>>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group label-floating">
                                        <label class="control-label"></label>
                                        <input type="text" class="form-control" value='<?php if($rezultati[$index]->Lab2 > 0) echo $rezultati[$index]->Lab2 ?>' name='<?php echo 'L2'.$cnt ?>' <?php if(!$L2) echo ' disabled' ?>>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group label-floating">
                                        <label class="control-label"></label>
                                        <input type="text" class="form-control" value='<?php if($rezultati[$index]->Lab3 > 0) echo $rezultati[$index]->Lab3 ?>' name='<?php echo 'L3'.$cnt ?>' <?php if(!$L3) echo ' disabled' ?>>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group label-floating">
                                        <label class="control-label"></label>
                                        <input type="text" class="form-control" value='<?php if($rezultati[$index]->Lab4 > 0) echo $rezultati[$index]->Lab4 ?>' name='<?php echo 'L4'.$cnt ?>' <?php if(!$L4) echo ' disabled' ?>>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group label-floating">
                                        <label class="control-label"></label>
                                        <input type="text" class="form-control" value='<?php if($rezultati[$index]->Lab5 > 0) echo $rezultati[$index]->Lab5 ?>' name='<?php echo 'L5'.$cnt ?>' <?php if(!$L5) echo ' disabled' ?>>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group label-floating">
                                        <label class="control-label"></label>
                                        <input type="text" class="form-control" value='<?php if($rezultati[$index]->DZ1 > 0) echo $rezultati[$index]->DZ1 ?>' name='<?php echo 'DZ1'.$cnt ?>' <?php if(!$DZ1) echo ' disabled' ?>>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group label-floating">
                                        <label class="control-label"></label>
                                        <input type="text" class="form-control" value='<?php if($rezultati[$index]->DZ2 > 0) echo $rezultati[$index]->DZ2 ?>' name='<?php echo 'DZ2'.$cnt ?>' <?php if(!$DZ2) echo ' disabled' ?>>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group label-floating">
                                        <label class="control-label"></label>
                                        <input type="text" class="form-control" value='<?php if($rezultati[$index]->DZ3 > 0) echo $rezultati[$index]->DZ3 ?>' name='<?php echo 'DZ3'.$cnt ?>' <?php if(!$DZ3) echo ' disabled' ?>>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group label-floating">
                                        <label class="control-label"></label>
                                        <input type="text" class="form-control" value='<?php if($rezultati[$index]->Projekat > 0) echo $rezultati[$index]->Projekat ?>' name='<?php echo 'PR'.$cnt ?>' <?php if(!$PR) echo ' disabled' ?>>
                                    </div>
                                </td>
                                                                                               
                                <td class="text-primary"><h3 class="title"><?php echo $zbir ?></h3></td>
                                <td class="text-warning"><h3 class="title"><?php echo $ocena ?></h3></td>
                            </tr>
                            <?php $cnt++; ?>
                        <?php } ?>
                        
                        
                    </tbody>
                </table>
                <button type="submit" class="btn btn-info pull-right">Potvrdi</button>
                <div class="clearfix"></div>
            </form>
            </div>
        </div>
    </div>
</div>
