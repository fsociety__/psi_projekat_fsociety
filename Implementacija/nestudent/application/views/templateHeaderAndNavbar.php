<!doctype html>
<html lang="en">
    
    <!-- Milan Lazarevic 617/14 -->
    
    <head>
        <meta charset="utf-8" />
        <link rel="apple-touch-icon" sizes="76x76" href=<?php echo base_url() . 'public/img/apple-icon.png' ?> />
        <link rel="icon" type="image/png" href=<?php echo base_url() . 'public/assets/img/favicon.png' ?> />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

        <title><?php $pageName?></title>

        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta name="viewport" content="width=device-width" />


        <!-- Bootstrap core CSS     -->
        <link href=<?php echo base_url() . 'public/css/bootstrap.min.css' ?> rel="stylesheet" />

        <!--  Material Dashboard CSS    -->
        <link href=<?php echo base_url() . 'public/css/material-dashboard.css' ?> rel="stylesheet"/>

        <!--  CSS for Demo Purpose, don't include it in your project     -->
        <link href=<?php echo base_url() . 'public/css/demo.css' ?> rel="stylesheet" />

        <!--     Fonts and icons     -->
        <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons' rel='stylesheet' type='text/css'>
    </head>

    <body>

        <div class="wrapper">
            <div class="sidebar" data-color="purple" data-image=<?php echo base_url() . 'public/img/sidebar-1.jpg' ?>>
                <!--
                Tip 1: You can change the color of the sidebar using: data-color="purple | blue | green | orange | red"

                Tip 2: you can also add an image using data-image tag
                -->

                <div class="logo">
                    <a href="<?php echo site_url('rasporedController/index/')?>" class="simple-text">
                        <img src=<?php echo base_url() . 'public/img/logo.png' ?> class="logo-nestudent" />
                    </a>
                </div>

                <div class="sidebar-wrapper">
                    <ul class="nav">
                        <?php if (strcmp("Raspored", $pageName)==0)
                                    $temp='class="active"';
                                  else 
                                     $temp="";
                        ?>
                        <li <?php echo $temp ?>>
                            <a href="<?php echo site_url('rasporedController/index/')?>">
                                <i class="material-icons">dashboard</i>
                                <p>Raspored</p>
                            </a>
                        </li>
                        
                        <?php if (strcmp("Materijali", $pageName)==0)
                                    $temp='class="active"';
                                  else 
                                     $temp="";
                        ?>
                        <li <?php echo $temp ?>>
                            <a href="<?php echo site_url('materijaliController/index/')?>">
                                <i class="material-icons">unarchive</i>
                                <p>Materijali</p>
                            </a>
                        </li>
                        <?php if (strcmp("Beleske", $pageName)==0)
                                    $temp='class="active"';
                                  else 
                                     $temp="";
                        ?>
                        <li <?php echo $temp ?>>
                            <a href="<?php echo site_url('beleskeController/index/')?>">
                                <i class="material-icons">content_paste</i>
                                <p>Beleske</p>
                            </a>
                        </li>
                         <?php if (strcmp("Rezultati", $pageName)==0)
                                    $temp='class="active"';
                                  else 
                                     $temp="";
                        ?>
                        <li <?php echo $temp?>>
                            <a href="<?php echo site_url('rezultatiController/index/')?>">
                                <i class="material-icons">library_books</i>
                                <p>Rezultati</p>
                            </a>
                        </li>
                        <?php if (strcmp("Biranje predmeta", $pageName)==0)
                                    $temp='class="active"';
                                  else 
                                     $temp="";
                        ?>
                        <li <?php echo $temp ?>>
                            <a href="<?php echo site_url('biranjePredmetaController/index/')?>">
                                <i class="material-icons">bubble_chart</i>
                                <p>Biranje predmeta</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="main-panel">
                <nav class="navbar navbar-transparent navbar-absolute">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <p class="navbar-brand"> <?php echo $userName ?></p>
                        </div>
                        <div class="collapse navbar-collapse">
                            <ul class="nav navbar-nav navbar-right">
                                
                                <li>
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="material-icons">person</i>
                                        <p class="hidden-lg hidden-md">Akaunt</p>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li><a href="<?php echo site_url('changePasswordController/')?>">Promeni lozinku</a></li>
                                        <li><a href="<?php echo site_url('loginController/logout/')?>">Izloguj se</a></li>
                                    </ul>
                                </li>
                            </ul>

                           
                        </div>
                    </div>
                </nav>

                <div class="content">
