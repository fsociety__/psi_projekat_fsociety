<!-- Milan Lazarevic 617/14 -->

<div class="container-fluid">
    
    
    <div class="row">
        <div class="navbar-brand pull-right"><font size="6"> <?php echo $predmetName; ?></font></div>
    </div>
    
    
    <?php if($userTip=='profesor'){
       echo
        '<div class="col-md-12">
            <div class="card card-plain">
                <div class="card-header" data-background-color="purple">
                    <h4 class="title">Otpremanje materijala</h4>
                    <p class="category"></p>
                </div>
                <div class="card-content table-responsive">
                    <table class="table table-hover">
                        <tbody id="ponedeljak">
                            <tr>
                                <td>
                                    ';?><?php echo form_open_multipart('materijaliController/upload');?><?php
                                        echo '<input type="file" name="userfile" size="20" />
                                        <br/><label>
                                        <input type="radio" name="tipNastave" value="P" checked>Predavanja</label><br><label>
                                        <input type="radio" name="tipNastave" value="V" >Vezbe</label><br>
                                        <br/>
                                        <input style="display:none" type="text" name="idp" value="';?><?php echo $predmet;?><?php echo '" />
                                        <input type="submit" value="otpremi" />
                                    </form>
                                </td>
                                <td>
                                    <p>';?><?php echo $poruka ?><?php echo '</p>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    
    
    ';}?>
    
    
    <div clas="row">  
            <div class="col-md-6">
                <div class="card card-plain">
                    <div class="card-header" data-background-color="green">
                        <h4 class="title">Predavanja</h4>
                        <p class="category"></p>
                    </div>
                    <div class="card-content table-responsive">
                        <table class="table table-hover">
                            <tbody id="ponedeljak">
                                <?php foreach($materijaliP as $row){
                                    echo '<tr><td>';
                                            echo $row->Naslov;
                                    echo'</td>
                                        <td class="td-actions text-right pull-right">
                                            ';
                                            echo form_open('materijaliController/download');
                                            echo '<button type="submit" rel="tooltip" title="Skini" class="btn btn-simple btn-xs">
                                                    <i class="material-icons">file_download</i>
                                                 </button>
                                                 <input type="text" style="display:none" name="idmat" value="'.$row->IDMat.'"/>
                                                 <input type="text" style="display:none" name="idp" value="'.$predmet.'"/>
                                            </form>';
                                            if($userTip=='profesor'){
                                                echo form_open('materijaliController/remove');         
                                                echo '<button type="submit" rel="tooltip" title="Obrisi" class="btn btn-danger btn-simple btn-xs">
                                                        <i class="material-icons">close</i>
                                                       </button>
                                                       <input type="text" style="display:none" name="idmat" value="'.$row->IDMat.'"/>
                                                       <input type="text" style="display:none" name="idp" value="'.$predmet.'"/>
                                                </form>';
                                            }
                                       echo' </td>
                                    </tr>';
                                }?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            
            <div class="col-md-6">
                <div class="card card-plain">
                    <div class="card-header" data-background-color="blue">
                        <h4 class="title">Vezbe</h4>
                        <p class="category"></p>
                    </div>
                    <div class="card-content table-responsive">
                        <table class="table table-hover">
                            <tbody id="ponedeljak">
                                <?php foreach($materijaliV as $row){
                                    echo '<tr><td>';
                                            echo $row->Naslov;
                                    echo'</td>
                                        <td class="td-actions text-right pull-right">
                                            ';
                                            echo form_open('materijaliController/download');
                                            echo '<button type="submit" rel="tooltip" title="Skini" class="btn btn-simple btn-xs">
                                                    <i class="material-icons">file_download</i>
                                                 </button>
                                                 <input type="text" style="display:none" name="idmat" value="'.$row->IDMat.'"/>
                                                 <input type="text" style="display:none" name="idp" value="'.$predmet.'"/>
                                            </form>';
                                            if($userTip=='profesor'){
                                                echo form_open('materijaliController/remove');         
                                                echo '<button type="submit" rel="tooltip" title="Obrisi" class="btn btn-danger btn-simple btn-xs">
                                                        <i class="material-icons">close</i>
                                                       </button>
                                                       <input type="text" style="display:none" name="idmat" value="'.$row->IDMat.'"/>
                                                       <input type="text" style="display:none" name="idp" value="'.$predmet.'"/>
                                                </form>';
                                            }
                                       echo' </td>
                                    </tr>';
                                }?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
    </div>
    
</div>