<!-- Emanuilo Jovanovic 563/14 -->

<div class="container-fluid">
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header" data-background-color="blue">
                    <h4 class="title">Biranje predmeta</h4>
                    <p class="category">Izabrati smer i godinu</p>
                </div>
                <div class="card-content">
                    
                        <?php echo form_open('biranjePredmetaController/dohvPredmete'); ?>
                    
                        <div class="row">
                            <div class="col-md-1">
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="smer" value="SI">
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <h4 class="card-title">SI</h4>
                            </div>

                            <div class="col-md-1">
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="smer" value="RTI">
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <h4 class="card-title">RTI</h4>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-1">
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="godina" value="I">
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <h4 class="card-title">I</h4>
                            </div>

                            <div class="col-md-1">
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="godina" value="II">
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <h4 class="card-title">II</h4>
                            </div>

                            <div class="col-md-1">
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="godina" value="III">
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <h4 class="card-title">III</h4>
                            </div>

                            <div class="col-md-1">
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="godina" value="IV">
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <h4 class="card-title">IV</h4>
                            </div>

                        </div>

                        <button type="submit" class="btn btn-info pull-left">Pretraga</button>
                        <div class="clearfix"></div>

                        
                    </form>
                </div>
            </div>
        </div>

    </div>
</div>
