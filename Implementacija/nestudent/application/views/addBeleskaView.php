<!--Aleksa Jankovic 2014 0569-->
<script>
    cbLine = 0;
    function toggleCB() {

        divTextArea = document.getElementById("divTextArea");
        divTextArea.hidden = !divTextArea.hidden;
        divSadrzajCB = document.getElementById("divSadrzajCB");
        divSadrzajCB.hidden = !divSadrzajCB.hidden;
        fakeToggle = document.getElementById("fakeToggle");
        fakeToggle.checked = !fakeToggle.checked;

        if (divTextArea.hidden) {
            cbLine = 0;
            textarea = document.getElementById("textarea");
            tekst = textarea.value.toString();

            niz = tekst.split("\n");
            tbody = document.getElementById("tbody");
            tbody.innerHTML = "";

            fakeRowCnt = document.getElementById("fakeRowCnt");
            fakeRowCnt.value = niz.length;

            for (i = 0; i < niz.length; i++) {
                // pravljenje tr-a tj jednog reda
                tr = document.createElement("tr");

                // pravljenje td-a 1 tj prve kolone
                td = document.createElement("td");
                // dodavanje checkbox-a u td i dodavanje td u tr
                div = document.createElement("div");
                div.setAttribute("class", "checkbox");
                // pravjenje checkbox-a
                input = document.createElement("input");
                input.setAttribute("type", "checkbox");
                input.setAttribute("name", "optionsCheckboxes" + cbLine);
                // pravljenje span1
                span1 = document.createElement("span");
                span1.setAttribute("class", "checkbox-material");
                // pravljenje span2
                span2 = document.createElement("span");
                span2.setAttribute("class", "check");
                // dodavanje span2 u span1
                span1.appendChild(span2);
                // pravljenje label-a
                label = document.createElement("label");
                // dodavanje input,span1,span2 u label
                label.appendChild(input);
                label.appendChild(span1);
                // dodavanje label u div
                div.appendChild(label);
                // dodavanje div u td
                td.appendChild(div);
                // dodavanje td u tr
                tr.appendChild(td);


                // pravljenje td-a 2
                td = document.createElement("td");
                // dodavanje inputa u td
                input = document.createElement("input");
                input.setAttribute("type", "text");
                input.setAttribute("class", "form-control");
                input.setAttribute("value", niz[i]);
                input.setAttribute("placeholder", "new Line");
                input.setAttribute("id", "cbLine" + cbLine);
                input.setAttribute("name", "cbLine" + cbLine);
                td.appendChild(input);
                // dodavanje td-a u tr
                tr.appendChild(td);


                // pravljenje td-a 3
                td = document.createElement("td");
                td.setAttribute("class", "td-actions text-right");
                td.setAttribute("width", "1");
                // pravljenje button-a 1
                button = document.createElement("a");
                button.setAttribute("rel", "tooltip");
                button.setAttribute("title", "Add Note");
                button.setAttribute("class", "btn btn-primary btn-simple btn-xs");
                button.setAttribute("onClick", "addCbRow()");
                // pravljenje iJebeni-a
                iJebeni = document.createElement("i");
                iJebeni.setAttribute("class", "material-icons");
                iJebeni.innerHTML = "add";
                // dodavanje iJebeni-a u button
                button.appendChild(iJebeni);
                // dodavanje button-a 1 u td
                td.appendChild(button);
                // pravljenje button-a 2
                button = document.createElement("button");
                button.setAttribute("rel", "tooltip");
                button.setAttribute("title", "Remove");
                button.setAttribute("class", "btn btn-danger btn-simple btn-xs");
                // button.setAttribute("onClick", "");
                // pravljenje iJebeni-a
                iJebeni = document.createElement("i");
                iJebeni.setAttribute("class", "material-icons");
                iJebeni.innerHTML = "close";
                // dodavanje iJebeni-a u button
                button.appendChild(iJebeni);
                // dodavanje button-a 1 u td
                // td.appendChild(button);                  // necemo remove ovo ima na jos 1 mesto u ovom dokumentu i na jos 2 mesta u editBeleskaView
                //dodavanje td-a u tr
                tr.appendChild(td);

                // nisi APENDOVAO TR U TBODY
                tbody.appendChild(tr); //sad jesam                
                cbLine++;
            }
        }

        if (divSadrzajCB.hidden) {
            strConcat = "";
            for (i = 0; i < cbLine; i++) {
                strConcat += document.getElementById("cbLine" + i).value;
                if (i + 1 !== cbLine) {
                    strConcat += '\n';
                }
            }
            document.getElementById("textarea").value = strConcat;
            cbLine = 0;
        }
    }

    function addCbRow() {
        tbody = document.getElementById("tbody");


        // pravljenje tr-a tj jednog reda
        tr = document.createElement("tr");

        // pravljenje td-a 1 tj prve kolone
        td = document.createElement("td");
        // dodavanje checkbox-a u td i dodavanje td u tr
        div = document.createElement("div");
        div.setAttribute("class", "checkbox");
        // pravjenje checkbox-a
        input = document.createElement("input");
        input.setAttribute("type", "checkbox");
        input.setAttribute("name", "optionsCheckboxes" + cbLine);
        // pravljenje span1
        span1 = document.createElement("span");
        span1.setAttribute("class", "checkbox-material");
        // pravljenje span2
        span2 = document.createElement("span");
        span2.setAttribute("class", "check");
        //dodavanje span2 u span1
        span1.appendChild(span2);
        // pravljenje label-a
        label = document.createElement("label");
        // dodavanje input,span1,span2 u label
        label.appendChild(input);
        label.appendChild(span1);
        // dodavanje label u div
        div.appendChild(label);
        // dodavanje div u td
        td.appendChild(div);
        // dodavanje td u tr
        tr.appendChild(td);


        // pravljenje td-a 2
        td = document.createElement("td");
        // dodavanje inputa u td
        input = document.createElement("input");
        input.setAttribute("type", "text");
        input.setAttribute("class", "form-control");
        input.setAttribute("value", "");
        input.setAttribute("placeholder", "new Line");
        input.setAttribute("id", "cbLine" + cbLine);
        input.setAttribute("name", "cbLine" + cbLine);
        td.appendChild(input);
        // dodavanje td-a u tr
        tr.appendChild(td);


        // pravljenje td-a 3
        td = document.createElement("td");
        td.setAttribute("class", "td-actions text-right");
        td.setAttribute("width", "1");
        // pravljenje button-a 1
        button = document.createElement("a");
        button.setAttribute("rel", "tooltip");
        button.setAttribute("title", "Add Note");
        button.setAttribute("class", "btn btn-primary btn-simple btn-xs");
        button.setAttribute("onClick", "addCbRow()");
        // pravljenje iJebeni-a
        iJebeni = document.createElement("i");
        iJebeni.setAttribute("class", "material-icons");
        iJebeni.innerHTML = "add";
        // dodavanje iJebeni-a u button
        button.appendChild(iJebeni);
        // dodavanje button-a 1 u td
        td.appendChild(button);
        // pravljenje button-a 2
        button = document.createElement("button");
        button.setAttribute("rel", "tooltip");
        button.setAttribute("title", "Remove");
        button.setAttribute("class", "btn btn-danger btn-simple btn-xs");
        // button.setAttribute("onClick", "");
        // pravljenje iJebeni-a
        iJebeni = document.createElement("i");
        iJebeni.setAttribute("class", "material-icons");
        iJebeni.innerHTML = "close";
        // dodavanje iJebeni-a u button
        button.appendChild(iJebeni);
        // dodavanje button-a 1 u td
        // td.appendChild(button);                 // necemo remove ovo ima na jos 1 mesto u ovom dokumentu i na jos 2 mesta u editBeleskaView
        //dodavanje td-a u tr
        tr.appendChild(td);



        // nisi APENDOVAO TR U TBODY
        tbody.appendChild(tr); // sad jesam        
        cbLine++;


        fakeRowCnt = document.getElementById("fakeRowCnt");
        fakeRowCnt.value++;
    }
</script>

<div class="container-fluid">    
    <?php echo form_open('beleskeController/saveBeleska'); ?>
    <div hidden>
        <input type="checkbox" name="fakeToggle" id="fakeToggle"/>
        <input type="number" name="fakeRowCnt" id="fakeRowCnt"/>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-12 col-lg-offset-3">
            <div class="card card-nav-tabs">
                <div class="card-header" data-background-color="green">
                    <div class="nav-tabs-navigation">
                        <div class="nav-tabs-wrapper">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group label-floating">
                                    <label class="control-label" style="color: white; font-size: medium">Naslov</label>
                                    <input type="text" class="form-control" style="color: white; font-size: large" id="naslov" name="naslov"/>                                    
                                </div>
                            </div>
                            <ul class="nav nav-tabs" data-tabs="tabs">
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="card-content">
                    <div class="tab-content">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="tab-pane active" id="divTextArea">
                                <textarea class="form-control" placeholder="Sadrzaj" rows="20" id="textarea" name="textarea"></textarea>
                            </div>
                            <!-- ovde ide tabela sa checkboxovima-->
                            <div class="tab-content" id="divSadrzajCB" hidden>
                                <div class="tab-pane active" id="profile">
                                    <table class="table">
                                        <tbody id="tbody">
                                            <!-- ode ce se generisati CB redovi -->
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <input type="button" class="btn btn-success" style="float: left" onClick="toggleCB()" value="Toggle">
                            <button type="submit" class="btn btn-success" style="float: right">Dodaj</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
</div>
