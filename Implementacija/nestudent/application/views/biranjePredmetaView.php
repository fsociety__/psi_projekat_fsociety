<!-- Emanuilo Jovanovic 563/14 -->

<div class="container-fluid">
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header" data-background-color="blue">
                    <h4 class="title">Biranje predmeta</h4>

                </div>
                <div class="card-content">
                    <?php echo form_open("biranjePredmetaController/upisiPredmete"); ?>
                        
                        <?php function daLiSlusaPredmet($idPre, $studSlus){
                            foreach($studSlus as $pred){
                                if($pred->IDPre == $idPre)
                                    return true;
                            }
                            return false;
                        } 
                        ?>
                        
                        <div class="tab-pane active" id="profile">
                            <table class="table">
                                <tbody>
                                    <?php foreach($predmeti as $index => $predmet) { ?>
                                    <tr>
                                        <td>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="predmeti[]" value='<?php echo $predmet->Naziv ?>' <?php if(daLiSlusaPredmet($predmet->IDPre, $studentSlusa)) echo 'checked' ?>>
                                                </label>
                                            </div>
                                        </td>
                                        <td><?php echo $predmet->Naziv ?></td>

                                    </tr>
                                    <?php } ?>
                                    
                                </tbody>
                            </table>
                        </div>


                        <button type="submit" class="btn btn-info pull-right">Potvrdi</button>
                        <div class="clearfix"></div>
                        <input type="text" value='<?php echo $smer ?>' name="smer" style="display:none">
                        <input type="text" value='<?php echo $godina ?>' name="godina" style="display:none">
                    </form>
                </div>
            </div>
        </div>

    </div>
</div>

