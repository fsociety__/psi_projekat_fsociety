<!-- Milan Lazarevic 617/14 -->

<div class="container-fluid">
    <div class="row">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header" data-background-color="purple">
                    <h4 class="title">Blokiranje</h4>
                    <p class="category"></p>
                </div>
                <div class="card-content">

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group label-floating">
                                    <label class="control-label">Email korisnika</label>
                                    <input type="text" class="form-control" name="email" id="email" required>
                                </div>
                            </div>
                        </div>
                        
                        
                        <button type="button" class="btn btn-primary pull-right" id="confBut">Blokiraj</button>
                        <div class="clearfix"></div>
                </div>
            </div>
        </div>
        
        
        <div class="col-md-4">
            <div class="card">
                <div class="card-header" data-background-color="purple">
                    <h4 class="title">Odblokiranje</h4>
                    <p class="category"></p>
                </div>
                <div class="card-content">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group label-floating">
                                    <label class="control-label">Email korisnika</label>
                                    <input type="text" class="form-control" name="email" id="email1" required>
                                </div>
                            </div>
                        </div>
                        
                        <button type="button" class="btn btn-primary pull-right" id="confBut1">Blokiraj</button>
                        <div class="clearfix"></div>
                </div>
            </div>
        </div>
        
        
        
    </div>
    
    <div class="row">
        <div class="col-md-4">
            <font size="6"><p id="povpor"></p></font>
        </div>
        <div class="col-md-4">
            <font size="6"><p id="povpor1"></p></font>
        </div>
    </div>
    
</div>
<script src=<?php echo base_url() . 'public/js/jquery-3.1.1.min.js' ?> type="text/javascript"></script>
 <script type="text/javascript">
    $(). ready(function(){
        $('#confBut').click(function(){
           var email=$('#email').val();
           if (email==""){
               $('#povpor').html("Polje mora biti popunjeno");
               return;
           }                  
           $.ajax({
               type: 'POST',
               data:{email: email},
               url: '<?php echo site_url('blokiranjeController/blokiraj'); ?>',
               success: function(result){
                   $('#povpor').html(result);
               }
           });
        });
    });

    $(). ready(function(){
        $('#confBut1').click(function(){
           var email=$('#email1').val();
           if (email==""){
               $('#povpor1').html("Polje mora biti popunjeno");
               return;
           }                  
           $.ajax({
               type: 'POST',
               data:{email: email},
               url: '<?php echo site_url('blokiranjeController/unblokiraj'); ?>',
               success: function(result){
                   $('#povpor1').html(result);
               }
           });
        });
    });       
</script>