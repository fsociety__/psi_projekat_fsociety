<!-- Emanuilo Jovanovic 563/14 -->

<div class="container-fluid">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header" data-background-color="blue">
                <h4 class="title">Formule</h4>
                <p class="category">Uneti formule za racunanje ocena</p>
            </div>
            <div class="card-content table-responsive">
                
                <?php echo form_open("biranjePredmetaController/unesiFormule"); ?> 
                    <?php 
                        function dohvFormulu($formulee, $idP, $idK){
                            if($formulee){
                                foreach($formulee as $form){
                                    if($form && $form->IDPre == $idP && $form->IDKor == $idK)
                                        return $form->Formula;
                                }
                            }
                        } 
                    ?>
                
                    <table class="table">
                        <thead class="text-info">
                            <div class="col-md-3">
                                <th><h4 class="title">Predmet</h4></th>
                            </div>
                            <div class="col-md-3">
                                <th class="text-primary"><h4 class="title">Formula</h4></th>
                            </div>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </thead>
                        <tbody>
                            <?php foreach($predmeti as $index => $predmet) { ?>
                                <tr>
                                    <td><h4 class="title"><?php echo $predmet->Naziv ?></h4></td>
                                    <td>
                                        <div class="form-group label-floating">
                                            <label class="control-label">e.g. K1*0.2+K2*0.2+K3*0.2+PR*0.4</label>
                                            <input type="text" class="form-control" name='<?php echo 'formula'.$index ?>' value='<?php echo dohvFormulu($formule, $predmet->IDPre, $idk) ?>'>
                                        </div>
                                    </td>
                                    <td> </td>
                                    <td> </td>
                                    <td> </td>
                                    <td> </td>

                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <button type="submit" class="btn btn-info pull-right">Potvrdi</button>
                    <div class="clearfix"></div>
                </form>
            </div>
        </div>
    </div>
</div>