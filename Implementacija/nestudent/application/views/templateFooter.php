<!-- Milan Lazarevic 617/14 -->


</div>

<footer class="footer">
    <div class="container-fluid">
        
        <p class="copyright pull-right">
            &copy; <script>document.write(new Date().getFullYear())</script> Ne:Student, za studente od studenata
        </p>
    </div>
</footer>
</div>
</div>

</body>

<!--   Core JS Files   -->
<script src="<?php echo base_url() . 'public/js/jquery-3.1.0.min.js' ?>" type="text/javascript"></script>
<script src=<?php echo base_url() . 'public/js/bootstrap.min.js' ?> type="text/javascript"></script>
<script src=<?php echo base_url() . 'public/js/material.min.js' ?> type="text/javascript"></script>

<!--  Charts Plugin -->
<script src=<?php echo base_url() . 'public/js/chartist.min.js' ?>></script>

<!--  Notifications Plugin    -->
<script src=<?php echo base_url() . 'public/js/bootstrap-notify.js' ?>></script>

<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>

<!-- Material Dashboard javascript methods -->
<script src=<?php echo base_url() . 'public/js/material-dashboard.js' ?>></script>

<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<script src=<?php echo base_url() . 'public/js/demo.js' ?>></script>

</html>