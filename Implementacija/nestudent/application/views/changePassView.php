<!-- Milan Lazarevic 617/14 -->

<div class="container-fluid">
    <div class="row">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header" data-background-color="purple">
                    <h4 class="title">Promena lozinke</h4>
                    <p class="category"></p>
                </div>
                <div class="card-content">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group label-floating">
                                    <label class="control-label">Stara lozinka</label>
                                    <input type="password" class="form-control" name="stari" id="stari" required>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group label-floating">
                                    <label class="control-label">Nova lozinka</label>
                                    <input type="password" class="form-control" name="novi" id="novi" required>
                                </div>
                            </div>
                        </div>
                        <font class="pull-left"><p id="povpor"></p></font>
                        <button type="button" class="btn btn-primary pull-right" id="confBut">Sačuvaj</button>
                        <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src=<?php echo base_url() . 'public/js/jquery-3.1.1.min.js' ?> type="text/javascript"></script>
 <script type="text/javascript">
                                    $(). ready(function(){
                                        $('#confBut').click(function(){
                                           var stari=$('#stari').val();
                                           var novi=$('#novi').val();
                                           if (stari=="" || novi==""){
                                               $('#povpor').html("Polja moraju biti popunjena");
                                               return;
                                           }                  
                                           $.ajax({
                                               type: 'POST',
                                               data:{stari: stari, novi: novi},
                                               url: '<?php echo site_url('changePasswordController/checkPassChange'); ?>',
                                               success: function(result){
                                                   $('#povpor').html(result);
                                               }
                                           });
                                        });
                                    });
                                    
                
</script>