<!--Aleksa Jankovic 2014 0569 frontend i backend deo-->
<!-- Milan Lazarevic 617/14 ajax-->

<!DOCTYPE html>
<html lang="en" class="perfect-scrollbar-on">    
    
    
<head>
    <meta charset="UTF-8">
    <link rel="apple-touch-icon" sizes="76x76" href=<?php echo base_url().'public/img/apple-icon.png' ?>/>
    <link rel="icon" type="image/png" href=<?php echo base_url().'public/img/favicon.png' ?>/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	
    <title>Registracija - Ne:Student</title>
	
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport"/>
    <meta name="viewport" content="width=device-width"/>

    <!-- Bootstrap core CSS     -->
    <link href=<?php echo base_url().'public/css/bootstrap.min.css'?> rel="stylesheet" />

    <!--  Material Dashboard CSS    -->
    <link href=<?php echo base_url().'public/css/material-dashboard-no-navbar.css'?> rel="stylesheet"/>

    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href=<?php echo base_url().'public/css/demo.css'?> rel="stylesheet" />

    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons" rel="stylesheet" type="text/css">
	
	
    <script type="text/javascript" charset="UTF-8" src=<?php echo base_url().'public/js/common.js'?>></script>
    <script type="text/javascript" charset="UTF-8" src=<?php echo base_url().'public/js/util.js'?>></script>
    <script type="text/javascript" charset="UTF-8" src=<?php echo base_url().'public/js/stats.js'?>></script>
</head>

<body>
    <nav class="navbar navbar-primary navbar-transparent navbar-absolute">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                    <a class="navbar-brand" href="<?php echo site_url('registerController/index/') ?>">
                            <img src=<?php echo base_url().'public/img/logo.png'?> class="logo-nestudent" />
                    </a>
            </div>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li class=" active ">
                        <?php echo anchor('registerController/index', '<i class="material-icons">person_add</i> Register','') ?>
                    </li>
                    <li class="">
                        <?php echo anchor('loginController/index', '<i class="material-icons">fingerprint</i> Login','') ?>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="wrapper wrapper-full-page">
        <div class="full-page register-page" filter-color="black" style=<?php echo '"background-image: url('.base_url().'public/img/register.jpeg);"'?>>
            <div class="content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            <div class="card card-signup">
                                <h2 class="card-title text-center">Registracija</h2>
                                <div class="row">

                                    <div class="col-md-8 col-md-offset-2">
                                        <?php //echo form_open('registerController/checkRegister'); ?>
                                            <div class="card-content">
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="material-icons">email</i>
                                                    </span>
                                                    <div class="form-group is-empty">
                                                        <input type="text" class="form-control" placeholder="Email..." name="email" id="email">
                                                        <span class="material-input"></span>
                                                    </div>
                                                </div>
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="material-icons">lock_outline</i>
                                                    </span>
                                                    <div class="form-group is-empty">
                                                        <input type="password" placeholder="Lozinka..." class="form-control" name="pass" id="pass">
                                                        <span class="material-input"></span>
                                                    </div>
                                                </div>
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="material-icons">lock_outline</i>
                                                    </span>
                                                    <div class="form-group is-empty">
                                                        <input type="password" placeholder="Ponovi lozinku..." class="form-control" name="passr" id="passr">
                                                        <span class="material-input"></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="footer text-center">
                                                <button type="button" class="btn btn-primary btn-round" id="confBut">Registruj se</button>
                                            </div>
                                            <div align="center"><p id="povpor"></p></div>
                                       <!-- </form> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="footer">
                <div class="container">
                    <nav class="pull-left">
                    </nav>
                    <p class="copyright pull-right">
                        Copyright
                        <script async="" src="./registerLex_files/analytics.js.download"></script><script>
                            document.write(new Date().getFullYear())
                        </script>
                         Ne:Student, za studente od studenata
                    </p>
                </div>
            </footer>
            <div class="full-page-background" style=<?php echo '"background-image: url('.base_url().'public/img/lock.jpeg);"'?>></div>
        </div>
    </div>

<!--   Core JS Files   -->
<script src=<?php echo base_url().'public/js/jquery-3.1.1.min.js'?> type="text/javascript"></script>
<script src=<?php echo base_url().'public/js/bootstrap.min.js'?> type="text/javascript"></script>
<script src=<?php echo base_url().'public/js/material.min.js'?> type="text/javascript"></script>
<script src=<?php echo base_url().'public/js/jquery-ui.min.js'?> type="text/javascript"></script>


<!-- Forms Validations Plugin -->
<script src=<?php echo base_url().'public/js/jquery.validate.min.js'?>></script>

<!--  Plugin for Date Time Picker and Full Calendar Plugin-->
<script src=<?php echo base_url().'public/js/moment.min.js'?>></script>

<!--  Charts Plugin -->
<script src=<?php echo base_url().'public/js/chartist.min.js'?>></script>

<!--  Plugin for the Wizard -->
<script src=<?php echo base_url().'public/js/jquery.bootstrap-wizard.js'?>></script>

<!--  Notifications Plugin    -->
<script src=<?php echo base_url().'public/js/bootstrap-notify.js'?>></script>

<!--   Sharrre Library    -->
<script src=<?php echo base_url().'public/js/jquery.sharrre.js'?>></script>

<!-- DateTimePicker Plugin -->
<script src=<?php echo base_url().'public/js/bootstrap-datetimepicker.js'?>></script>

<!-- Vector Map plugin -->
<script src=<?php echo base_url().'public/js/jquery-jvectormap.js'?>></script>

<!-- Sliders Plugin -->
<script src=<?php echo base_url().'public/js/nouislider.min.js'?>></script>

<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>

<!-- Select Plugin -->
<script src=<?php echo base_url().'public/js/jquery.select-bootstrap.js'?>></script>

<!--  DataTables.net Plugin    -->
<script src=<?php echo base_url().'public/js/jquery.datatables.js'?>></script>

<!-- Sweet Alert 2 plugin -->
<script src=<?php echo base_url().'public/js/sweetalert2.js'?>></script>

<!--	Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
<script src=<?php echo base_url().'public/js/jasny-bootstrap.min.js'?>></script>

<!--  Full Calendar Plugin    -->
<script src=<?php echo base_url().'public/js/fullcalendar.min.js'?>></script>

<!-- TagsInput Plugin -->
<script src=<?php echo base_url().'public/js/jquery.tagsinput.js'?>></script>

<!-- Material Dashboard javascript methods -->
<script src=<?php echo base_url().'public/js/material-dashboard.js'?>></script>

<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<script src=<?php echo base_url().'public/js/demo.js'?>></script>

<script type="text/javascript">
    $().ready(function() {
        demo.checkFullPageBackgroundImage();

        setTimeout(function() {
            // after 1000 ms we add the class animated to the login/register card
            $('.card').removeClass('card-hidden');
        }, 700)
     });    
        
        
    $(). ready(function(){
        $('#confBut').click(function(){
           var email=$('#email').val();
           var pass=$('#pass').val();
           var passr=$('#passr').val();
           $.ajax({
               type: 'POST',
               data:{email: email, pass: pass, passr: passr},
               url: '<?php echo site_url('registerController/checkRegister'); ?>',
               success: function(result){
                   if (result=='Uspeh'){
                       window.location='<?php echo site_url('loginController/index'); ?>';
                   }
                   $('#povpor').html(result);
               }
           });
        });
    });

   
</script>

</body></html>