<?php
//Emanuilo Jovanovic 563/14

class predmetModel extends CI_Model{
    
    public function dohvPredmete($smer , $godina){
        
        //dohvatanje IDSme na osnovu naziva smera
        $this->db->select('IDSme');
        $this->db->from('smer');
        $this->db->where('Naziv', $smer);
        
        $query=$this->db->get();
        $row = $query->row();
        $idSmer = null;
        if(isset($row)){
            $idSmer = $row->IDSme;
        }
        
        //dohvatanje IDGod na osnovu broja godine 
        $this->db->select('IDGod');
        $this->db->from('godina');
        $this->db->where('Vrednost', $godina);
        
        $query=$this->db->get();
        $row = $query->row();
        $idGod = null;
        if(isset($row)){
            $idGod = $row->IDGod;
        }
        
        //dohvatanje naziva predmeta na tom smeru na toj godini
        $query = $this->db->query("SELECT p.*
                                   FROM `predmet` AS p, `jena` AS j
                                   WHERE p.IDPre = j.IDPre AND j.IDGod =".$idGod."  AND j.IDSme =".$idSmer."
                                   ORDER BY p.IDPre ASC");

        return $query->result();
    }
    
    public function dohvNazivPredmeta($idPre){
        $this->db->select('Naziv');
        $this->db->from('predmet');
        $this->db->where('IDPre', $idPre);
        $query = $this->db->get();
        
        return $query->row()->Naziv;
    }
    
    public function dohvIdPredmeta($naziv){
        $this->db->select('IDPre');
        $this->db->from('predmet');
        $this->db->where('Naziv', $naziv);
        $query = $this->db->get();
        
        return $query->row()->IDPre;
    }
}