<!--Aleksa Jankovic 2014 0569-->
<?php

class beleskeModel extends CI_Model {

    public function getBeleske($idKor) {

        //dohvatanje Beleski
        $this->db->select('IDBel, Naslov, isCheckBox');
        $this->db->from('beleska');
        $this->db->where('IDKor', $idKor);
        $this->db->order_by("IDBel", "desc");
        $query1 = $this->db->get();

        $beleske = [];
        $cntBeleske = 0;
        foreach ($query1->result() as $beleska) {
            $this->db->select('IDSta, isChecked, Tekst');
            $this->db->from('stavka');
            $this->db->where('IDBel', $beleska->IDBel);
            $this->db->order_by("IDSta", "asc");
            $query2 = $this->db->get();
            $stavke = [];
            $cntStavke = 0;
            foreach ($query2->result() as $stavka) {
                $stavke[$cntStavke] = array(
                    'IDSta' => $stavka->IDSta,
                    'isChecked' => $stavka->isChecked,
                    'Tekst' => $stavka->Tekst);
                $cntStavke++;
            }

            if (isset($stavke)) {
                $beleske[$cntBeleske] = array(
                    'IDBel' => $beleska->IDBel,
                    'Naslov' => $beleska->Naslov,
                    'isCheckBox' => $beleska->isCheckBox,
                    'stavke' => $stavke);
            } else {
                $beleske[$cntBeleske] = array(
                    'IDBel' => $beleska->IDBel,
                    'Naslov' => $beleska->Naslov,
                    'isCheckBox' => $beleska->isCheckBox);
            }

            $cntBeleske++;
        }
        return $beleske;
    }

    public function insertTextBeleska($idKor, $idPre, $naslov, $isCheckBox, $textarea) {
        if (!isset($idKor) || !isset($idPre) || !isset($textarea)) {
            //naslov namerno izostavljen iz provere
            return;
        }

        $dataBeleska = Array(
            'IDKor' => $idKor,
            'IDPre' => $idPre,
            'Naslov' => $naslov,
            'isCheckBox' => $isCheckBox
        );
        $this->db->insert('beleska', $dataBeleska);

        //nalazenje IDBel nove beleske
        $idBel = $this->db->insert_id();

        $dataStavka = Array(
            'IDBel' => $idBel,
            'Tekst' => $textarea,
            'isChecked' => 0
        );
        $this->db->insert('stavka', $dataStavka);
    }

    public function insertCheckBeleska($idKor, $idPre, $naslov, $isCheckBox, $stavke, $isChecked) {
        if (!isset($idKor) || !isset($idPre) || !isset($stavke)) {
            return;
        }

        $dataBeleska = Array(
            'IDKor' => $idKor,
            'IDPre' => $idPre,
            'Naslov' => $naslov,
            'isCheckBox' => $isCheckBox
        );
        $this->db->insert('beleska', $dataBeleska);

        //nalazenje IDBel nove beleske
        $idBel = $this->db->insert_id();

        for ($i = 0; $i < count($stavke); $i++) {
            if ($stavke[$i] == "") {
                continue;
            }
            $dataStavka = Array(
                'IDBel' => $idBel,
                'Tekst' => $stavke[$i],
                'isChecked' => $isChecked[$i]
            );
            $this->db->insert('stavka', $dataStavka);
        }
    }

    public function verifyBeleskaOwership($idBel, $idKor) {
        $this->db->select('IDKor');
        $this->db->from('beleska');
        $this->db->where('IDBel', $idBel);
        $query = $this->db->get();
        $row = $query->row();

        return $row->IDKor == $idKor;
    }

    public function fetchBeleska($idBel) {
        $this->db->select('IDBel, IDKor, IDPre, Naslov, isCheckBox');
        $this->db->from('beleska');
        $this->db->where('IDBel', $idBel);
        $query1 = $this->db->get();
        $beleska = $query1->row();

        $this->db->select('IDSta, isChecked, Tekst');
        $this->db->from('stavka');
        $this->db->where('IDBel', $beleska->IDBel);
        $this->db->order_by("IDSta", "asc");
        $query2 = $this->db->get();
        $stavke = [];
        $cntStavke = 0;
        foreach ($query2->result() as $stavka) {
            $stavke[$cntStavke] = array(
                'IDSta' => $stavka->IDSta,
                'isChecked' => $stavka->isChecked,
                'Tekst' => $stavka->Tekst);
            $cntStavke++;
        }

        if (isset($stavke)) {
            $beleskaStavke = array(
                'IDBel' => $beleska->IDBel,
                'IDPre' => $beleska->IDPre,
                'Naslov' => $beleska->Naslov,
                'isCheckBox' => $beleska->isCheckBox,
                'stavke' => $stavke);
        } else {
            $beleskaStavke = array(
                'IDBel' => $beleska->IDBel,
                'IDPre' => $beleska->IDPre,
                'Naslov' => $beleska->Naslov,
                'isCheckBox' => $beleska->isCheckBox);
        }

        return $beleskaStavke;
    }

    public function deleteBeleska($idBel) {
        $this->db->where('IDBel', $idBel);
        $this->db->delete('beleska');
        $this->db->where('IDBel', $idBel);
        $this->db->delete('stavka');
    }

}
