<?php

//Milan Lazarevic 617/14
//Emanuilo Jovanovic 563/14

class korisnikModel extends CI_Model{
    
    public function login($email, $pass){
        $this->db->select('Email, Lozinka, Blokiran');
        $this->db->from('korisnik');
        $this->db->where('Email', $email);
        $this->db->where('Lozinka', $pass);
        $query=$this->db->get();
        if ($query->num_rows()==1){
            $blokiran=$query->row();
            if ($blokiran->Blokiran==1)
                return "Korisnik je blokiran";
            return "uspesno";
        }
        else
            return "Pogrešni email i lozinka";
    }
    
    public function create($email, $pass){
        $this->db->select('Email');
        $this->db->from('korisnik');
        $this->db->where('Email', $email);
        $query=$this->db->get();
        
        if ($query->num_rows()==1)
            return "Email već postoji";
        else{
            $user_data;
            list($user, $domain) = explode('@', $email);
            
            if ($domain == 'student.etf.rs') { //korisnik je student
                if(strlen($user) != 9)
                    return false;
                $godinaUpisa = (int)substr($user, 2, 2);
                $brojIndeksa = (int)substr($user, 4, 4);
                
                $user_data=Array(
                    'Email' => $email,
                    'Lozinka' => $pass,
                    'Tip' => 'student',
                    'GodinaUpisa' => $godinaUpisa,
                    'Indeks' => $brojIndeksa
                );
            }
            else if ($domain == 'etf.rs'){  //TODO: admin
                $user_data=Array(
                    'Email' => $email,
                    'Lozinka' => $pass,
                    'Tip' => 'profesor',
                    'Blokiran' => 0
                );
            }
            else 
                return "Email nije odgovarajućeg domena";
            
            $insert=$this->db->insert('korisnik', $user_data);
            if ($insert){
                return "uspesno";
            }
            else{
                return "Došlo je do greške";
            }
        }
    }
    
    public function getId($email){
        $this->db->select('IDKor');
        $this->db->from('korisnik');
        $this->db->where('Email', $email);
        $row = $this->db->get()->row();
        return $row->IDKor;
    }
    
    public function getTip($email){
        $this->db->select('Tip');
        $this->db->from('korisnik');
        $this->db->where('Email', $email);
        $row = $this->db->get()->row();
        return $row->Tip;
    }
    
    public function changePass($stari, $novi, $id){
        $this->db->select('Lozinka');
        $this->db->from('korisnik');
        $this->db->where('IDKor', $id);
        $query=$this->db->get();
        if ($query->num_rows()!=1){
            return false;
        }
        else{
            if (strcmp($query->row()->Lozinka, $stari)==0){
                $data= array(
                    'Lozinka' => $novi
                );
                $this->db->where('IDKor', $id);
                return $this->db->update('korisnik', $data);
            }
            else{
                return false;
            }
        }
    }
    
    
    public function blokiraj($email){
        $this->db->select('Blokiran');
        $this->db->from('korisnik');
        $this->db->where('Email', $email);
        $query=$this->db->get();
        if ($query->num_rows()!=1){
            return false;
        }
        $query=$query->row();
        if ($query->Blokiran==1){
            return false;
        }
        else{
            $data= array(
                'Blokiran' => 1
            );
            $this->db->where('Email', $email);
            return $this->db->update('korisnik', $data);
        }
    }
    
    public function unblokiraj($email){
        $this->db->select('Blokiran');
        $this->db->from('korisnik');
        $this->db->where('Email', $email);
        $query=$this->db->get();
        if ($query->num_rows()!=1){
            return false;
        }
        $query=$query->row();
        $pom=$query->Blokiran+1;
        if ($pom==1){
            return false;
        }
        else{
            $data= array(
                'Blokiran' => 0
            );
            $this->db->where('Email', $email);
            return $this->db->update('korisnik', $data);
        }
    }
    
}