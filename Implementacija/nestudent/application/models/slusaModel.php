<?php

//Milan Lazarevic 617/14
//Emanuilo Jovanovic 563/14

class slusaModel extends CI_Model{
    public function sveAktivnosti($idStu){
        
         $query = $this->db->query("SELECT slusa.P1 as 'BP1', slusa.P2 as 'BP2', slusa.P3 as 'BP3', slusa.V1 as 'BV1', slusa.V2 as 'BV2', slusa.V3 as 'BV3', slusa.IDPre as 'IdPre', predmet.P1, predmet.P2, predmet.P3, predmet.V1, predmet.V2, predmet.V3, predmet.Naziv as 'Naziv'
                                   FROM `slusa` AS slusa, `predmet` AS predmet
                                   WHERE predmet.IDPre = slusa.IDPre  AND slusa.IDKor =".$idStu);
        
        
        //$this->db->select("slusa.P1 as 'BP1', slusa.P2 as 'BP2', slusa.P3 as 'BP3', slusa.V1 as 'BV1', slusa.V2 as 'BV2', slusa.V3 as 'BV3', slusa.IDPre as 'IdPre'");
        //$this->db->select('predmet.Naziv, predmet.P1, predmet.P2, predmet.P3, predmet.V1, predmet.V2, predmet.V3');
        //$this->db->from('slusa');
        //$this->db->where('IDKor', $idStu);
        //$this->db->join('predmet', 'predmet.IDPre = slusa.IDPre');
        //$query=$this->db->get();
        return $query->result();
    }
    
    public function slusaPredmete($predmeti, $idStud, $smer, $godina){
        if($predmeti){
            //ubacivanje u bazu koje predmete je student izabrao
            foreach ($predmeti as $predmet){
                $this->db->select('IDPre');
                $this->db->from('predmet');
                $this->db->where('Naziv', $predmet);
                $query=$this->db->get();
                $row = $query->row();
                $idPred = $row->IDPre;

                //provera da li u bazi vec postoji podatak da student slusa taj predmet
                $this->db->select('*');
                $this->db->from('slusa');
                $this->db->where('IDKor', $idStud);
                $this->db->where('IDPre', $idPred);
                $query=$this->db->get();

                if ($query->num_rows() != 1){
                    $data = array(
                        'IDKor' => $idStud,
                        'IDPre' => $idPred
                    );
                    $this->db->insert('slusa', $data);
                }

            }
        }
        
        //brisanje iz baze odstikliranih predmeta, rezultata za te predmete i formule
        $slusaPredmete = $this->predmetiStudGodSmer($idStud, $smer, $godina);
        foreach($slusaPredmete as $slusa){
            $slusaIDalje = false;
            if($predmeti){
                foreach($predmeti as $pred){
                    if($slusa->Naziv == $pred)
                        $slusaIDalje = true;
                }
            }
            
            if($slusaIDalje == false){
                $this->db->where('IDPre', $slusa->IDPre);
                $this->db->where('IDKor', $idStud);
                $this->db->delete('slusa');
                
                $this->db->where('IDPre', $slusa->IDPre);
                $this->db->where('IDKor', $idStud);
                $this->db->delete('rezultat');
                
                $this->db->where('IDPre', $slusa->IDPre);
                $this->db->where('IDKor', $idStud);
                $this->db->delete('formula');
            }
        }
    }
    
    public function dohvPredmete($idStud){
        //dohvata predmete koje student slusa
        $query = $this->db->query("SELECT p.*
                                   FROM `predmet` AS p, `slusa` AS s
                                   WHERE p.IDPre = s.IDPre AND s.IDKor =".$idStud."
                                   ORDER BY p.IDPre ASC");
        
        return $query->result();
    }
    
    public function promenaAktivnosti($idP, $idK, $p1, $p2, $p3, $v1 , $v2, $v3){
        $data= array(
            'P1' => $p1,
            'P2' => $p2,
            'P3' => $p3,
            'V1' => $v1,
            'V2' => $v2,
            'V3' => $v3
        );
        
        $this->db->where('IDKor', $idK);
        $this->db->where('IDPre', $idP);
        
        return $this->db->update('slusa', $data);
    }
    
    public function studentiNaPredmetu($idPre){
        //dohvata studente koji slusaju predmet
        $query = $this->db->query("SELECT k.*
                                   FROM `korisnik` AS k, `slusa` AS s
                                   WHERE k.IDKor = s.IDKor AND k.Tip = 'student' AND s.IDPre =".$idPre."
                                   ORDER BY k.GodinaUpisa ASC, k.Indeks ASC");
        
        return $query->result();
    }
    
    public function predmetiStudGodSmer($idKor, $smer, $god){
        $query = $this->db->query("SELECT p.*
                                   FROM `predmet` AS p, `slusa` AS s, `jena` AS j, `smer` AS sm, `godina` AS g 
                                   WHERE p.IDPre = s.IDPre AND s.IDKor =".$idKor." AND j.IDPre = p.IDPre AND j.IDSme = sm.IDSme AND sm.Naziv = '".$smer."' AND j.IDGod = g.IDGod AND G.Vrednost =".$god."
                                   ORDER BY p.IDPre ASC");
        
        return $query->result();
    }
}