<?php
//Emanuilo Jovanovic 563/14

class rezultatModel extends CI_Model{
    
    public function kreirajRezultate($predmeti, $idKor, $tip){
        if(!$predmeti || $tip != 'student')
            return;
        
        foreach ($predmeti as $predmet){
            $this->db->select('IDPre');
            $this->db->from('predmet');
            $this->db->where('Naziv', $predmet);
            $query=$this->db->get();
            $row = $query->row();
            $idPred = $row->IDPre;
            
//            $data = array(
//                'IDKor' => $idKor,
//                'IDPre' => $idPred,
//                'Kolokvijum1' => 0,
//                'Kolokvijum2' => 0,
//                'Kolokvijum3' => 0,
//                'Lab1' => 0,
//                'Lab2' => 0,
//                'Lab3' => 0,
//                'Lab4' => 0,
//                'Lab5' => 0,
//                'Projekat' => 0,
//                'DZ1' => 0,
//                'DZ2' => 0,
//                'DZ3' => 0,
//            );
//
//            $this->db->insert('rezultat', $data);
            
            $query = $this->db->query("INSERT IGNORE INTO `rezultat`
                                       SET `IDKor` =".$idKor.", "
                                        . "`IDPre` =".$idPred.", "
                                        . "`Kolokvijum1` = 0, "
                                        . "`Kolokvijum2` = 0, "
                                        . "`Kolokvijum3` = 0, "
                                        . "`Lab1` = 0, "
                                        . "`Lab2` = 0, "
                                        . "`Lab3` = 0, "
                                        . "`Lab4` = 0, "
                                        . "`Lab5` = 0, "
                                        . "`Projekat` = 0, "
                                        . "`DZ1` = 0, "
                                        . "`DZ2` = 0, "
                                        . "`DZ3` = 0 ");
        }
    }
    
    public function dohvRezultate($idKor){
        //dohvatanje rezultata svih predmeta za jednog korisnika
        $this->db->select("*");
        $this->db->from('rezultat');
        $this->db->where('IDKor', $idKor);
        $this->db->order_by('IDPre', 'asc');
        
        $query=$this->db->get();

        return $query->result();
    }
    
    public function dohvRezultatePredmeta($idPre){
        //dohvatanje rezultata svih studenata iz jednog predmeta
        $query = $this->db->query("SELECT r.*
                                   FROM `rezultat` AS r, `korisnik` AS k
                                   WHERE r.IDPre =".$idPre." AND r.IDKor = k.IDKor
                                   ORDER BY k.GodinaUpisa ASC, k.Indeks ASC");
        
        return $query->result();
    }
    
    public function azurirajRezultat($idKor, $idPre, $K1, $K2, $K3, $L1, $L2, $L3, $L4, $L5, $DZ1, $DZ2, $DZ3, $PR){
        $data = array(
            'Kolokvijum1' => $K1,
            'Kolokvijum2' => $K2,
            'Kolokvijum3' => $K3,
            'Lab1' => $L1,
            'Lab2' => $L2,
            'Lab3' => $L3,
            'Lab4' => $L4,
            'Lab5' => $L5,
            'DZ1' => $DZ1,
            'DZ2' => $DZ2,
            'DZ3' => $DZ3,
            'Projekat' => $PR,
        );
        
        $this->db->where('IDKor', $idKor);
        $this->db->where('IDPre', $idPre);
        $this->db->update('rezultat', $data);
    }
}
