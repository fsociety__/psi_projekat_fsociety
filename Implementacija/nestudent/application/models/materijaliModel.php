<?php

//Milan Lazarevic 617/14

class materijaliModel extends CI_Model{
    public function ubaciMaterijal($naziv, $lokacija, $pv, $idp){
        $data = array(
            'Naslov' => $naziv,
            'Lokacija' => $lokacija,
            'PV' => $pv,
            'IDPre' => $idp
        );
        return $this->db->insert('materijal', $data);
    }
    
    public function dohvatiMaterijale($idp, $pv){
        $this->db->select('IDMat, Naslov');
        $this->db->from('materijal');
        $this->db->where('IDPre', $idp);
        $this->db->where('PV', $pv);
        $query=$this->db->get();
        return $query->result();
    }
    
    public function dohvatiLokaciju($idmat){
        $this->db->select('Lokacija');
        $this->db->from('materijal');
        $this->db->where('IDMat', $idmat);
        $query=$this->db->get();
        return $query->row();
    }
    
    public function obrisi($id){
        return $this->db->delete('materijal', array('IDMat' => $id)); 
    }
    
}