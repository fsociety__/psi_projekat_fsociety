<!--Aleksa Jankovic 2014 0569-->
<?php

class beleskeController extends CI_Controller {

    // TODO idKor za sada hardkodovano
    public function getIdKor() {
        return $this->session->userdata('idk');
    }

    // TODO idPre za sada hardkodovano
    public function getIdPre() {
        return 1;
    }

    public function index() {
        $this->getBeleske();
    }

    // index calls this method
    public function getBeleske() {
        $this->load->model('beleskeModel');
        $idKor = $this->getIdKor();
        $niz['beleske'] = $this->beleskeModel->getBeleske($idKor);

        if (!$this->session->has_userdata('email'))
            redirect('loginController/index');
        $data['pageName'] = 'Beleske';
        $data['userName'] = $this->session->userdata('email');
        $data['userTip'] = $this->session->userdata('tip');
        $this->load->view('templateHeaderAndNavbar', $data);
        $this->load->view('beleskeView', $niz);
        $this->load->view('templateFooter');
    }

    // called when user presses NOVA BELESKA
    public function addBeleska() {

        $this->form_validation->set_rules('sadrzaj', 'Sadrzaj', 'required');

        if (!$this->session->has_userdata('email'))
            redirect('loginController/index');
        $data['pageName'] = 'Beleske';
        $data['userName'] = $this->session->userdata('email');
        $data['userTip'] = $this->session->userdata('tip');
        $this->load->view('templateHeaderAndNavbar', $data);
        $this->load->view('addBeleskaView');
        $this->load->view('templateFooter');
    }

    // called when user presses the edit pen next to a note
    public function editBeleska($idBel) {

        $this->load->model('beleskeModel');
        if (!$this->beleskeModel->verifyBeleskaOwership($idBel, $this->getIdKor())) {
            redirect('beleskeController/index');
        }

        //dovuci belesku
        $beleska = $this->beleskeModel->fetchBeleska($idBel);
        $niz['beleska'] = $beleska;
        //prosledi je editView-u

        $this->form_validation->set_rules('sadrzaj', 'Sadrzaj', 'required');

        if (!$this->session->has_userdata('email'))
            redirect('loginController/index');
        $data['pageName'] = 'Beleske';
        $data['userName'] = $this->session->userdata('email');
        $data['userTip'] = $this->session->userdata('tip');
        $this->load->view('templateHeaderAndNavbar', $data);
        $this->load->view('editBeleskaView', $niz);
        $this->load->view('templateFooter');
    }

    // called when user presses DODAJ to save a new Beleska
    public function saveBeleska() {
        $this->load->model('beleskeModel');
        $idKor = $this->getIdKor();
        // TODO idPre za sada hardkodovano
        $idPre = $this->getIdPre();
        $naslov = $this->input->post('naslov');
        $isCheckBox = $this->input->post('fakeToggle');
        if (isset($isCheckBox)) {
            $isCheckBox = 1;
            $fakeRowCnt = $this->input->post('fakeRowCnt');
            $stavke = [];
            $isChecked = [];
            for ($i = 0; $i < $fakeRowCnt; $i++) {
                $stavke[$i] = $this->input->post('cbLine' . $i);
                $isChecked[$i] = $this->input->post('optionsCheckboxes' . $i);
                if (isset($isChecked[$i])) {
                    $isChecked[$i] = 1;
                } else {
                    $isChecked[$i] = 0;
                }
            }
            $this->beleskeModel->insertCheckBeleska($idKor, $idPre, $naslov, $isCheckBox, $stavke, $isChecked);
        } else {
            $isCheckBox = 0;
            $textarea = $this->input->post('textarea');
            $this->beleskeModel->insertTextBeleska($idKor, $idPre, $naslov, $isCheckBox, $textarea);
        }

        redirect('beleskeController/index');
    }

    // called when user presses SACUVAJ upon editing an existing Beleska
    public function reSaveBeleska($idBel, $idPre) {
        $this->load->model('beleskeModel');
        $idKor = $this->getIdKor();
        $naslov = $this->input->post('naslov');
        $isCheckBox = $this->input->post('fakeToggle');
        if (isset($isCheckBox)) {
            $isCheckBox = 1;
            $fakeRowCnt = $this->input->post('fakeRowCnt');
            $stavke = [];
            $isChecked = [];
            for ($i = 0; $i < $fakeRowCnt; $i++) {
                $stavke[$i] = $this->input->post('cbLine' . $i);
                $isChecked[$i] = $this->input->post('optionsCheckboxes' . $i);
                if (isset($isChecked[$i])) {
                    $isChecked[$i] = 1;
                } else {
                    $isChecked[$i] = 0;
                }
            }
            $this->beleskeModel->insertCheckBeleska($idKor, $idPre, $naslov, $isCheckBox, $stavke, $isChecked);            
        } else {
            $isCheckBox = 0;
            $textarea = $this->input->post('textarea');
            $this->beleskeModel->insertTextBeleska($idKor, $idPre, $naslov, $isCheckBox, $textarea);
        }
        $this->beleskeModel->deleteBeleska($idBel);
        redirect('beleskeController/index');        
    }
    
    public function deleteBeleska($idBel){
        $this->load->model('beleskeModel');
        if (!$this->beleskeModel->verifyBeleskaOwership($idBel, $this->getIdKor())) {
            redirect('beleskeController/index');
        }
        
        $this->beleskeModel->deleteBeleska($idBel);
        redirect('beleskeController/index');
    }

}
