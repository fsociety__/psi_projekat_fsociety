<?php

//Emanuilo Jovanovic 563/14

class rezultatiController extends CI_Controller{
    
    public function index(){
         if (!$this->session->has_userdata('email'))
            redirect('loginController/index');
        $data['pageName']='Rezultati';
        $data['userName']=$this->session->userdata('email');
        $data['userTip']=$this->session->userdata('tip');
        $this->load->view('templateHeaderAndNavbar', $data);
        
        //dohvatanje idKor iz sesije
        $idKor = $this->session->userdata('idk');
		
        $tip = $this->session->userdata('tip');      
        if($tip == 'student'){
            //dohvatanje predmeta koje student slusa
            $this->load->model('slusaModel');
            $niz['predmeti'] = $this->slusaModel->dohvPredmete($idKor);

            //dohvatanje rezultata za studenta
            $this->load->model('rezultatModel');
            $niz['rezultati'] = $this->rezultatModel->dohvRezultate($idKor);

            //dohvatanje formula za racunanje ocena predmeta koje student slusa
            $this->load->model('formulaModel');
            $formule = array();
            foreach($niz['predmeti'] as $predmet){
                $formule[] = $this->formulaModel->dohvFormulu($predmet->IDPre);
            }
            $niz['formule'] = $formule;

            $this->load->view('rezultatiView', $niz);
            $this->load->view('templateFooter');
        }
        else{ //profesor
            $this->izborPredmetaProf();
        }
    }
    
    public function izborPredmetaProf(){
        //dohvatanje idKor iz sesije
        $this->load->model('korisnikModel');
        $idKor = $this->korisnikModel->getId($this->session->userdata('email'));
        
        //dohvatanje predmeta koje profesor drzi
        $this->load->model('slusaModel');
        $niz['predmeti'] = $this->slusaModel->dohvPredmete($idKor);
        $niz['adresa'] = "rezultatiController/indexProf";
        
        $this->load->view('izabratiPredmetView', $niz);
        $this->load->view('templateFooter');
        
    }
    
    public function indexProf($idPre){
        if (!$this->session->has_userdata('email'))
            redirect('loginController/index');
        $data['pageName']='Rezultati';
        $data['userName']=$this->session->userdata('email');
        $data['userTip']=$this->session->userdata('tip');
        $this->load->view('templateHeaderAndNavbar', $data);
        
        
        $this->load->model('predmetModel');
        $niz['predmet'] = $this->predmetModel->dohvNazivPredmeta($idPre);
        
        $this->load->model('slusaModel');
        $niz['studenti'] = $this->slusaModel->studentiNaPredmetu($idPre);
        
        $this->load->model('rezultatModel');
        $niz['rezultati'] = $this->rezultatModel->dohvRezultatePredmeta($idPre);

        $this->load->model('formulaModel');
        $niz['formula'] = $this->formulaModel->dohvFormulu($idPre);
        
        
        $this->load->view('rezultatiProfesorView', $niz);
        $this->load->view('templateFooter');
        
    }
    
    public function indexStud(){
        
    }
    
    public function unesiRezultate(){
        $this->load->model('korisnikModel');
        $idKor = $this->korisnikModel->getId($this->session->userdata('email'));
        
        $this->load->model('slusaModel');
        $predmeti = $this->slusaModel->dohvPredmete($idKor);
        
        //azuriranje rezultata svih predmeta
        foreach ($predmeti as $ind => $predmet){
            $index = $ind + 1;
            $idPre = $predmet->IDPre;
            $K1 = $this->input->post('K1'.$index);
            $K2 = $this->input->post('K2'.$index);
            $K3 = $this->input->post('K3'.$index);
            $L1 = $this->input->post('L1'.$index);
            $L2 = $this->input->post('L2'.$index);
            $L3 = $this->input->post('L3'.$index);
            $L4 = $this->input->post('L4'.$index);
            $L5 = $this->input->post('L5'.$index);
            $DZ1 = $this->input->post('DZ1'.$index);
            $DZ2 = $this->input->post('DZ2'.$index);
            $DZ3 = $this->input->post('DZ3'.$index);
            $PR = $this->input->post('PR'.$index);
            
            $this->load->model('rezultatModel');
            $this->rezultatModel->azurirajRezultat($idKor, $idPre, $K1, $K2, $K3, $L1, $L2, $L3, $L4, $L5, $DZ1, $DZ2, $DZ3, $PR);
        }
        
        $this->index();
    }
    
    public function unesiRezultateProfesor(){
        $imePredmeta = $this->input->post('imePredmeta');
        $this->load->model('predmetModel');
        $idPre = $this->predmetModel->dohvIdPredmeta($imePredmeta);
        
        $this->load->model('slusaModel');
        $studenti = $this->slusaModel->studentiNaPredmetu($idPre);
        
        //azuriranje rezultata svih studenata na predmetu
        foreach ($studenti as $ind => $student){
            $index = $ind + 1;
            $idKor = $student->IDKor;
            $K1 = $this->input->post('K1'.$index);
            $K2 = $this->input->post('K2'.$index);
            $K3 = $this->input->post('K3'.$index);
            $L1 = $this->input->post('L1'.$index);
            $L2 = $this->input->post('L2'.$index);
            $L3 = $this->input->post('L3'.$index);
            $L4 = $this->input->post('L4'.$index);
            $L5 = $this->input->post('L5'.$index);
            $DZ1 = $this->input->post('DZ1'.$index);
            $DZ2 = $this->input->post('DZ2'.$index);
            $DZ3 = $this->input->post('DZ3'.$index);
            $PR = $this->input->post('PR'.$index);
            
            $this->load->model('rezultatModel');
            $this->rezultatModel->azurirajRezultat($idKor, $idPre, $K1, $K2, $K3, $L1, $L2, $L3, $L4, $L5, $DZ1, $DZ2, $DZ3, $PR);
        }
        
        $this->indexProf($idPre);
    }
}
