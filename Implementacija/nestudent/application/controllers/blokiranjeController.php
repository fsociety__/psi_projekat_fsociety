<?php

//Milan Lazarevic 617/14

class blokiranjeController extends CI_Controller{
    public function index($poruka=""){
        if (!$this->session->has_userdata('email'))
            redirect('loginController/index');
        $data['pageName'] = 'Blokiranje';
        $data['userName'] = $this->session->userdata('email');
        $data['userTip'] = $this->session->userdata('tip');
        $this->load->view('templateHeaderAndNavbarAdmin', $data);
        $data['poruka']=$poruka;
        $this->load->view('blokiranjeView', $data);
        $this->load->view('templateFooter');
    }
    
    public function blokiraj(){
        $email=$this->input->post('email');
        $this->load->model('korisnikModel');
        if ($this->korisnikModel->blokiraj($email))
            echo "Korisnik je uspešno blokiran";
        else
            echo "Došlo je do greške";
    } 
    
    public function unblokiraj(){
        $email=$this->input->post('email');
        $this->load->model('korisnikModel');
        if ($this->korisnikModel->unblokiraj($email))
            echo "Korisnik je uspešno odblokiran";
        else
            echo "Došlo je do greške";
    } 
    
}
