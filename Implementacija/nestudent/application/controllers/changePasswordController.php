<?php

//Milan Lazarevic 617/14

class changePasswordController extends CI_Controller {
    
    public function index(){
        if (!$this->session->has_userdata('email'))
            redirect('loginController/index');
        $data['userName']= $this->session->userdata('email');
        $data['pageName']="Promena passworda";
        $data['userTip']= $this->session->userdata('tip');
        $this->load->view('templateHeaderAndNavbar', $data);
        $this->load->view('changePassView', $data);
        $this->load->view('templateFooter');
    }
    
    public function checkPassChange(){
        $stari = $this->input->post('stari');
        $novi = $this->input->post('novi');
        $this->load->model('korisnikModel');
        $id=intval($this->session->userdata('idk'));
        if ($this->korisnikModel->changePass($stari, $novi, $id)){
            if (!$this->session->has_userdata('email'))
                redirect('loginController/index');
            echo "Lozinka je promenjena";
        }
        else{
             if (!$this->session->has_userdata('email'))
                redirect('loginController/index');
            echo "Doslo je do greske";
        }
    }
    
}