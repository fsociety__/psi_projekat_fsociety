<?php

//Milan Lazarevic 617/14


class registerController extends CI_Controller{
    public function index(){
        $this->load->view('registerView');
    }
    
    public function checkRegister(){
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email',array('required' => 'Potrebno je uneti email', 'valid_email' => 'Email nije ispravan'));
        $this->form_validation->set_rules('pass', 'Lozinka', 'required|callback_checkRegistration',array('required' => 'Potrebno je uneti lozinku'));
        
        if ($this->form_validation->run()==false){
            echo validation_errors();
        }
        else {
            echo "Uspeh";
            //redirect('loginController/index');
        }         
    }
    
    public function checkRegistration(){
        $email = $this->input->post('email');
        $pass = $this->input->post('pass');
        $passr = $this->input->post('passr');
        if ($email==null)
            return true;
        
        if ($pass==null){
            return true;
        }
        
        if (strcmp($pass, $passr)!=0){
            $this->form_validation->set_message('checkRegistration', "Lozinke se ne poklapaju");
            return false;
        }
        $this->load->model('korisnikModel');
        $pom=$this->korisnikModel->create($email,$pass);
        if ($pom=='uspesno'){
            return true;
        }
        else{
            $this->form_validation->set_message('checkRegistration', $pom);
            return false;
        }
    }
    
    
}
