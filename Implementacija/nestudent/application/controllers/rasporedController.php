<?php

//Milan Lazarevic 617/14

class rasporedController extends CI_Controller {



    public function index() {

        if (!$this->session->has_userdata('email'))
            redirect('loginController/index');
        $this->load->model('slusaModel');
        $pom=intval($this->session->userdata('idk'));
        $data['info'] = $this->slusaModel->sveAktivnosti($pom);
        
        
        $data['userName']= $this->session->userdata('email');
        $data['pageName']="Raspored";
        $data['userTip']= $this->session->userdata('tip');
        $this->load->view('templateHeaderAndNavbar', $data);
        
        $this->load->view('rasporedIndexView', $data);

        $this->load->view('templateFooter');

    }

    

    public function izmeniRaspored(){
        if (!$this->session->has_userdata('email'))
            redirect('loginController/index');
        $this->load->model('slusaModel');
        $data['userName']= $this->session->userdata('email');
        $data['pageName']="Raspored";
        $data['userTip']= $this->session->userdata('tip');
        $this->load->view('templateHeaderAndNavbar', $data);
        $pom=intval($this->session->userdata('idk'));
        $data['info']=$this->slusaModel->sveAktivnosti($pom);

        $this->load->view('rasporedIzmeniView', $data);

        $this->load->view('templateFooter');

    }
    
    public function sacuvajRaspored(){
        if ($this->input->post()){
            $this->load->model('slusaModel');
            $ident = explode("_",$this->input->post('identifikacija'));
            $bp1= explode("_",$this->input->post('bp1'));
            $bp2= explode("_",$this->input->post('bp2'));
            $bp3= explode("_",$this->input->post('bp3'));
            $bv1= explode("_",$this->input->post('bv1'));
            $bv2= explode("_",$this->input->post('bv2'));
            $bv3= explode("_",$this->input->post('bv3'));
            
            $idk=intval($this->session->userdata('idk'));
            
            $len=sizeof($ident);
            $len--;
            for ($i=0;$i<$len;$i++){
                $this->slusaModel->promenaAktivnosti($ident[$i], $idk, $bp1[$i], $bp2[$i], $bp3[$i], $bv1[$i] , $bv2[$i], $bv3[$i]);
                //echo $ident[$i]." a:".$bp1[$i]." b:".$bp2[$i]." c:".$bp3[$i]." d:".$bv1[$i]." e:".$bv2[$i]." f:".$bv3[$i];
            }
            
            redirect('rasporedController/index');
        }
        else{
            echo "error 404";
        }
    }

}

