<?php
//Emanuilo Jovanovic 563/14

class biranjePredmetaController extends CI_Controller {

    public function index() {
        if (!$this->session->has_userdata('email'))
            redirect('loginController/index');
        $data['pageName']='Biranje predmeta';
        $data['userName']=$this->session->userdata('email');
        $data['userTip']=$this->session->userdata('tip');
        $this->load->view('templateHeaderAndNavbar', $data);
        $this->load->view('biranjeSmeraIGodineView');
        $this->load->view('templateFooter');
    }
    
    public function dohvPredmete(){
        $smer = $this->input->post('smer');
        $godina = $this->input->post('godina');
        $idKor = $this->session->userdata('idk');
        $brojGodine;
        if($godina == 'I')
            $brojGodine = 1;
        else if($godina == 'II')
            $brojGodine = 2;
        else if($godina == 'III')
            $brojGodine = 3;
        else
            $brojGodine = 4;
        
        //dohvatanje predmeta iz selektovanog smera i godine
        $this->load->model('predmetModel');
        $niz['predmeti'] = $this->predmetModel->dohvPredmete($smer, $brojGodine);
        //dohvatanje predmeta koje student slusa na toj godin na tom smeru
        $this->load->model('slusaModel');
        $niz['studentSlusa'] = $this->slusaModel->predmetiStudGodSmer($idKor, $smer, $brojGodine);
        $niz['smer'] = $smer;
        $niz['godina'] = $brojGodine;
        
        
        if (!$this->session->has_userdata('email'))
            redirect('loginController/index');
        $data['pageName']='Biranje predmeta';
        $data['userName']=$this->session->userdata('email');
        $data['userTip']=$this->session->userdata('tip');
        $this->load->view('templateHeaderAndNavbar', $data);
        $this->load->view('biranjePredmetaView', $niz);
        $this->load->view('templateFooter');
    }
    
    public function upisiPredmete(){
        $idKor = $this->session->userdata('idk');
        
        //dohvatanje selektovanih predmeta iz post zahteva i upis u bazu
        $predmeti = $this->input->post('predmeti');
        $smer = $this->input->post('smer');
        $godina = $this->input->post('godina');
        $this->load->model('slusaModel');
        $this->slusaModel->slusaPredmete($predmeti, $idKor, $smer, $godina);

        //kreiranje rezultata za trenutnog korisnika i izabrane predmete
        $this->load->model('rezultatModel');
        $this->rezultatModel->kreirajRezultate($predmeti, $idKor, $this->session->userdata('tip'));
        
        
        //dohvatanje postojecih formula
        $niz['predmeti'] = $this->slusaModel->dohvPredmete($idKor);
        $this->load->model('formulaModel');
        $formule = array();
        foreach($niz['predmeti'] as $pr){
            $formule[] = $this->formulaModel->dohvFormulu($pr->IDPre);    
        }
        $niz['formule'] = $formule;
        $niz['idk'] = $this->session->userdata('idk');
        
        //otvaranje novog prozora za unos formula za racunanje ocena
        if (!$this->session->has_userdata('email'))
            redirect('loginController/index');
        $data['pageName']='Biranje predmeta';
        $data['userName']=$this->session->userdata('email');
        $data['userTip']=$this->session->userdata('tip');
        $this->load->view('templateHeaderAndNavbar', $data);
        $this->load->view('unosFormulaView', $niz);
        $this->load->view('templateFooter');
    }
    
    public function unesiFormule(){
        $this->load->model('korisnikModel');
        $idKor = $this->session->userdata('idk');
        
        $this->load->model('slusaModel');
        $predmeti = $this->slusaModel->dohvPredmete($idKor);
        
        //unos formula u bazu podataka
        foreach ($predmeti as $index => $predmet){
            $this->load->model('formulaModel');
            $this->formulaModel->unesiFormulu($predmet->IDPre, $idKor, $this->input->post('formula'.$index));
        }
        
        redirect('rasporedController/index');
    }
}
