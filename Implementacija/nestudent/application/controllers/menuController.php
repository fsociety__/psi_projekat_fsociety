<?php

//Milan Lazarevic 617/14

class menuController extends CI_Controller {
    public function index(){
        if (!$this->session->has_userdata('tip'))
            redirect('loginController/index');
        if ($this->session->userdata('tip')=='admin'){
            redirect('blokiranjeController/index');
        }
        else{
            redirect('rasporedController/index');
        }
    }
}