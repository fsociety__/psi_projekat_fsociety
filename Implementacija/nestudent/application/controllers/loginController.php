<?php

//Milan Lazarevic 617/14

class loginController extends CI_Controller {

    public function index() {
        $this->load->view('loginView');
    }

    public function checkLogin() {
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email',array('required' => 'Potrebno je uneti email', 'valid_email' => 'Email nije ispravan'));
        $this->form_validation->set_rules('pass', 'Lozinka', 'required|callback_checkUser',array('required' => 'Potrebno je uneti lozinku'));

        if ($this->form_validation->run() == false) {
            echo validation_errors();
        } else {
            $email = $this->input->post('email');
            $this->session->set_userdata('email', $email);
            $this->load->model('korisnikModel');
            $idk=$this->korisnikModel->getId($email);
            $idt=$this->korisnikModel->getTip($email);
            $this->session->set_userdata('idk', $idk);
            $this->session->set_userdata('tip', $idt);
            echo "Uspeh";
            //redirect('menuController/index');
        }
    }

    public function checkUser() {
        $email = $this->input->post('email');
        $pass = $this->input->post('pass');
        $this->load->model('korisnikModel');
        $rezultat=$this->korisnikModel->login($email, $pass);
        if ($rezultat=='uspesno'){
            return true;
        } else {
            $this->form_validation->set_message('checkUser', $rezultat);
            return false;
        }
    }
    
    public function logout(){
        $this->session->sess_destroy();
        redirect('loginController/index');
    }
    
}
