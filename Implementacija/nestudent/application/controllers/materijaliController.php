<?php


class materijaliController extends CI_Controller{
    public function index(){
         if (!$this->session->has_userdata('email'))
            redirect('loginController/index');
        $data['pageName']='Materijali';
        $data['userName']=$this->session->userdata('email');
        $data['userTip']=$this->session->userdata('tip');
        $this->load->view('templateHeaderAndNavbar', $data);
        
        //dohvatanje idKor iz sesije
        $this->load->model('korisnikModel');
        $idKor =$this->session->userdata('idk');
        
        //dohvatanje predmeta
        $this->load->model('slusaModel');
        $niz['predmeti'] = $this->slusaModel->dohvPredmete($idKor);
        $niz['adresa'] = "materijaliController/pregledMaterijala";
        
        $this->load->view('izabratiPredmetView', $niz);
        $this->load->view('templateFooter');
    }
    
    public function pregledMaterijala($idPre, $poruka=""){
        if (!$this->session->has_userdata('email'))
            redirect('loginController/index');
        $data['pageName']='Materijali';
        $data['userName']=$this->session->userdata('email');
        $data['userTip']=$this->session->userdata('tip');
        $this->load->model('materijaliModel');
        $this->load->model('predmetModel');
        $data['predmetName']=$this->predmetModel->dohvNazivPredmeta($idPre);
        $this->load->view('templateHeaderAndNavbar', $data);
        $data['materijaliP']=$this->materijaliModel->dohvatiMaterijale($idPre,'P');
        $data['materijaliV']=$this->materijaliModel->dohvatiMaterijale($idPre,'V');
        $data['predmet']=$idPre;
        $data['poruka']=$poruka;
        $this->load->view('materijaliView', $data);
        $this->load->view('templateFooter');
    }
    
    public function upload(){
        $config['upload_path']          = './uploads/';
        $config['allowed_types']        = 'pdf|zip|txt|doc|ppt|docx';
        $config['max_size']             = 100;
        $config['max_width']            = 1024;
        $config['max_height']           = 768;
        
        $idp=$this->input->post('idp');
        
        
        $this->load->library('upload', $config);

        $poruka="Neuspesno";
        if ($this->upload->do_upload('userfile')){
            $this->load->model('materijaliModel');
            $nameM=$this->upload->data('file_name');
            $lokacijaM=$this->upload->data('full_path');
            $pv=$this->input->post('tipNastave');
            if ($this->materijaliModel->ubaciMaterijal($nameM, $lokacijaM, $pv, $idp))
                $poruka="Uspesno";
        }
        
        redirect('materijaliController/pregledMaterijala/'.$idp.'/'.$poruka);
        
       // if (!$this->session->has_userdata('email'))
      //      redirect('loginController/index');
      //  $data['pageName']='Materijali';
      //  $data['userName']=$this->session->userdata('email');
      //  $data['userTip']=$this->session->userdata('tip');
      //  $this->load->view('templateHeaderAndNavbar', $data);
      //  $data['predmet']=$idp;
      //  $data['poruka']=$poruka;
      //  $this->load->model('predmetModel');
      //  $this->load->model('materijaliModel');
      //  $data['materijaliP']=$this->materijaliModel->dohvatiMaterijale($idp,'P');
      //  $data['materijaliV']=$this->materijaliModel->dohvatiMaterijale($idp,'V');
      //  $data['predmetName']=$this->predmetModel->dohvNazivPredmeta($idp);
      //  $this->load->view('materijaliView',$data);
       // $this->load->view('templateFooter');
    }
    
    
    public function download(){
        $this->load->helper('download');
        $id=$this->input->post('idmat');
        $this->load->model('materijaliModel');
        $location=$this->materijaliModel->dohvatiLokaciju($id);
        $location=$location->Lokacija;
        force_download($location, NULL);
        
        $idp=$this->input->post('idp');
        
        redirect('materijaliController/pregledMaterijala/'.$idp);
    }
    
    public function remove(){
        $id=$this->input->post('idmat');
        $this->load->model('materijaliModel');
        $location=$this->materijaliModel->dohvatiLokaciju($id);
        $location=$location->Lokacija;
        
        if (file_exists($location)){
            if (unlink($location)){
                $this->materijaliModel->obrisi($id);
            }
        }
        else{
            $this->materijaliModel->obrisi($id);
        }
        
        $idp=$this->input->post('idp');
        
        redirect('materijaliController/pregledMaterijala/'.$idp);
    }
    
}
