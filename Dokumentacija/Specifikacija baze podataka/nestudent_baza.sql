-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 15, 2017 at 10:00 PM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nestudent_baza`
--

-- --------------------------------------------------------

--
-- Table structure for table `beleska`
--

CREATE TABLE `beleska` (
  `IDKor` int(11) NOT NULL,
  `IDPre` int(11) DEFAULT NULL,
  `IDBel` int(11) NOT NULL,
  `Naslov` varchar(50) DEFAULT NULL,
  `isCheckBox` bit(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `formula`
--

CREATE TABLE `formula` (
  `IDPre` int(11) NOT NULL,
  `IDKor` int(11) NOT NULL,
  `Formula` varchar(60) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `godina`
--

CREATE TABLE `godina` (
  `IDGod` int(11) NOT NULL,
  `Vrednost` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `godina`
--

INSERT INTO `godina` (`IDGod`, `Vrednost`) VALUES
(1, 1),
(2, 2),
(3, 3),
(4, 4);

-- --------------------------------------------------------

--
-- Table structure for table `jena`
--

CREATE TABLE `jena` (
  `IDSme` int(11) NOT NULL,
  `IDGod` int(11) NOT NULL,
  `IDPre` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jena`
--

INSERT INTO `jena` (`IDSme`, `IDGod`, `IDPre`) VALUES
(2, 1, 7),
(1, 2, 6),
(1, 3, 1),
(1, 3, 2),
(1, 3, 3),
(1, 3, 4),
(1, 3, 5);

-- --------------------------------------------------------

--
-- Table structure for table `korisnik`
--

CREATE TABLE `korisnik` (
  `IDKor` int(11) NOT NULL,
  `Email` varchar(60) NOT NULL,
  `GodinaUpisa` int(11) DEFAULT NULL,
  `Indeks` int(11) DEFAULT NULL,
  `Lozinka` varchar(50) NOT NULL,
  `Tip` varchar(20) NOT NULL,
  `Blokiran` bit(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `materijal`
--

CREATE TABLE `materijal` (
  `IDMat` int(11) NOT NULL,
  `IDPre` int(11) NOT NULL,
  `Lokacija` varchar(120) NOT NULL,
  `Naslov` varchar(50) DEFAULT NULL,
  `PV` varchar(2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `predmet`
--

CREATE TABLE `predmet` (
  `IDPre` int(11) NOT NULL,
  `P1` varchar(30) DEFAULT NULL,
  `P2` varchar(30) DEFAULT NULL,
  `P3` varchar(30) DEFAULT NULL,
  `V1` varchar(30) DEFAULT NULL,
  `V2` varchar(30) DEFAULT NULL,
  `V3` varchar(30) DEFAULT NULL,
  `Naziv` varchar(70) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `predmet`
--

INSERT INTO `predmet` (`IDPre`, `P1`, `P2`, `P3`, `V1`, `V2`, `V3`, `Naziv`) VALUES
(1, '1_08:00', '5_08:00', NULL, '2_12:00', '5_12:00', NULL, 'Konkurentno i distribuirano programiranje'),
(2, '5_10:00', NULL, NULL, '3_08:00', NULL, NULL, 'Infrastruktura za elektronsko poslovanje'),
(3, '1_14:00', NULL, NULL, '3_20:00', NULL, NULL, 'Softverski alati baza podataka'),
(4, '1_12:00', '4_08:00', NULL, '2_14:00', '4_10:00', NULL, 'Principi softverskog inzenjerstva'),
(5, '1_10:00', NULL, NULL, '3_10:00', NULL, NULL, 'Sistemski softver'),
(6, '3_14:00', NULL, NULL, '4_16:00', NULL, NULL, 'Veb dizajn'),
(7, '1_16:00', '2_16:00', NULL, '3_16:00', '5_18:00', NULL, 'Algoritmi i strukture podataka 1'),
(8, '1_18:00', NULL, NULL, '5_20:00', NULL, NULL, 'Osnovi elektrotehnike');

-- --------------------------------------------------------

--
-- Table structure for table `rezultat`
--

CREATE TABLE `rezultat` (
  `IDKor` int(11) NOT NULL,
  `IDPre` int(11) NOT NULL,
  `Kolokvijum1` float DEFAULT NULL,
  `Kolokvijum2` float DEFAULT NULL,
  `Kolokvijum3` float DEFAULT NULL,
  `Lab1` float DEFAULT NULL,
  `Lab2` float DEFAULT NULL,
  `Lab3` float DEFAULT NULL,
  `Lab4` float DEFAULT NULL,
  `Lab5` float DEFAULT NULL,
  `Projekat` float DEFAULT NULL,
  `DZ1` float DEFAULT NULL,
  `DZ2` float DEFAULT NULL,
  `DZ3` float DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `slusa`
--

CREATE TABLE `slusa` (
  `IDKor` int(11) NOT NULL,
  `IDPre` int(11) NOT NULL,
  `P1` tinyint(4) DEFAULT NULL,
  `P2` tinyint(4) DEFAULT NULL,
  `P3` tinyint(4) DEFAULT NULL,
  `V1` tinyint(4) DEFAULT NULL,
  `V2` tinyint(4) DEFAULT NULL,
  `V3` tinyint(4) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `smer`
--

CREATE TABLE `smer` (
  `IDSme` int(11) NOT NULL,
  `Naziv` varchar(60) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `smer`
--

INSERT INTO `smer` (`IDSme`, `Naziv`) VALUES
(1, 'SI'),
(2, 'RTI');

-- --------------------------------------------------------

--
-- Table structure for table `stavka`
--

CREATE TABLE `stavka` (
  `IDBel` int(11) NOT NULL,
  `IDSta` int(11) NOT NULL,
  `Tekst` longtext,
  `isChecked` bit(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `beleska`
--
ALTER TABLE `beleska`
  ADD PRIMARY KEY (`IDBel`),
  ADD KEY `R_6` (`IDKor`),
  ADD KEY `R_7` (`IDPre`);

--
-- Indexes for table `formula`
--
ALTER TABLE `formula`
  ADD PRIMARY KEY (`IDPre`,`IDKor`);

--
-- Indexes for table `godina`
--
ALTER TABLE `godina`
  ADD PRIMARY KEY (`IDGod`);

--
-- Indexes for table `jena`
--
ALTER TABLE `jena`
  ADD PRIMARY KEY (`IDGod`,`IDSme`,`IDPre`),
  ADD KEY `R_19` (`IDSme`),
  ADD KEY `R_21` (`IDPre`);

--
-- Indexes for table `korisnik`
--
ALTER TABLE `korisnik`
  ADD PRIMARY KEY (`IDKor`);

--
-- Indexes for table `materijal`
--
ALTER TABLE `materijal`
  ADD PRIMARY KEY (`IDMat`),
  ADD KEY `R_15` (`IDPre`);

--
-- Indexes for table `predmet`
--
ALTER TABLE `predmet`
  ADD PRIMARY KEY (`IDPre`),
  ADD UNIQUE KEY `Naziv` (`Naziv`);

--
-- Indexes for table `rezultat`
--
ALTER TABLE `rezultat`
  ADD PRIMARY KEY (`IDKor`,`IDPre`),
  ADD KEY `R_9` (`IDPre`);

--
-- Indexes for table `slusa`
--
ALTER TABLE `slusa`
  ADD PRIMARY KEY (`IDKor`,`IDPre`),
  ADD KEY `R_5` (`IDPre`);

--
-- Indexes for table `smer`
--
ALTER TABLE `smer`
  ADD PRIMARY KEY (`IDSme`);

--
-- Indexes for table `stavka`
--
ALTER TABLE `stavka`
  ADD PRIMARY KEY (`IDSta`),
  ADD KEY `R_16` (`IDBel`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `beleska`
--
ALTER TABLE `beleska`
  MODIFY `IDBel` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `godina`
--
ALTER TABLE `godina`
  MODIFY `IDGod` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `korisnik`
--
ALTER TABLE `korisnik`
  MODIFY `IDKor` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `materijal`
--
ALTER TABLE `materijal`
  MODIFY `IDMat` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `predmet`
--
ALTER TABLE `predmet`
  MODIFY `IDPre` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `smer`
--
ALTER TABLE `smer`
  MODIFY `IDSme` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `stavka`
--
ALTER TABLE `stavka`
  MODIFY `IDSta` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
